# CircleCI config
You need to make sure the following is configured:

  1. [AWS credentials](https://circleci.com/docs/2.0/deployment-integrations/#aws) (`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`) for an account that has:
    1. access defined in the IAM policy defined at the bottom of this README
  1. `PROD_BUCKET` env var for the production S3 bucket name, e.g: `oeh.example.com`
  1. `STAGING_BUCKET` env var for the staging S3 bucket name, e.g: `staging.oeh.example.com`
  1. `ROLLBAR_ACCESS_TOKEN` for notifying [Rollbar](https://docs.rollbar.com/reference#section-authentication) of a deployment
  1. `PROD_OEH_API_KEY` env var for the production API key
  1. `STAGING_OEH_API_KEY` env var for the staging API key
  1. `OEH_GOOGLE_MAPS_API_KEY` env var for the Google Maps API key, otherwise you get the warning about being "for dev use only"
  1. `PROD_CLOUDFRONT_DIST_ID` env var as the AWS CloudFront distribution ID so we can invalidate the cache after deploy
  1. `STAGING_CLOUDFRONT_DIST_ID` env var as the AWS CloudFront distribution ID so we can invalidate the cache after deploy

Set up the S3 buckets for deployment. Run the full set of commands for each bucket you want to create. Run the following `awscli` commands (replace `oeh.example.com` with your bucket name):

  1. create the bucket

          aws s3api create-bucket \
            --bucket oeh.example.com \
            --region ap-southeast-2 \
            --create-bucket-configuration LocationConstraint=ap-southeast-2

  1. if you want the quick and dirty website solution, configure the bucket for website hosting using the following 3 steps:
  1. delete the "public access block" so you will be allowed to set public access on objects

          aws s3api delete-public-access-block \
            --bucket=oeh.example.com

  1. set a bucket policy to allow public access

          aws s3api put-bucket-policy --bucket=oeh.example.com --policy='{
            "Version": "2012-10-17",
              "Id": "Policy1427772347182",
              "Statement": [{
                "Sid": "Stmt1427772340560",
                "Effect": "Allow",
                "Principal": "*",
                "Action": "s3:GetObject",
                "Resource": "arn:aws:s3:::oeh.example.com/*"
              }]
          }'

  1. configure the bucket for website hosting

          aws s3api put-bucket-website \
            --bucket oeh.example.com \
            --website-configuration '{"IndexDocument":{"Suffix":"index.html"}}'

  1. or, if you want to host the website properly, create a CloudFront distribution that pulls from the S3 bucket

          TODO write instructions for this, but as a summary:
          - enter a CNAME that you'll use in Route53, like staging.oeh.example.com
          - use the cert your created in us-east-1 in ACM
          - add a comment, it'll be easier to find your distro in lists later
          - default root object is index.html
          - origin is the S3 bucket, no extra path is needed
          - answer 'yes' to "Restrict bucket access"
          - redirect HTTP to HTTPS
          - use the defaults for the rest

  1. if you deployed the CloudFront distribution, add basic HTTP security by deploying the Lambda@Edge function. Follow the instructions at http://kynatro.com/blog/2018/01/03/a-step-by-step-guide-to-creating-a-password-protected-s3-bucket/. The code for the function is `aws/http-basic-auth-lambda.js`. Note, you need to create this function in us-east-1.

## IAM policy
If you want to create an AWS user specifically for the CircleCI job, give it the following policy:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:GetObjectAcl",
                "s3:DeleteObject",
                "s3:PutObjectAcl",
                "s3:PutObject",
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::staging.oeh.example.com",
                "arn:aws:s3:::staging.oeh.example.com/*",
                "arn:aws:s3:::oeh.example.com",
                "arn:aws:s3:::oeh.example.com/*"
            ]
        }
    ]
}
```
...be sure to update the resource ARNs (`staging.oeh.example.com` and `oeh.example.com`) in the policy to match your bucket names.
