#!/bin/bash
# performs an invalidation on the CloudFront distribution so the new code is ready straight away.
# before running, be sure to set the CLOUDFRONT_DIST_ID env var to a dist ID like E3BB7QK8WXCEXH
set -e

if [ -z "$CLOUDFRONT_DIST_ID" ]; then
  echo "[INFO] no cloudfront distribution ID defined, skipping invalidation."
  exit 0
fi

echo "[INFO] invalidating cache for dist ID=$CLOUDFRONT_DIST_ID"
aws cloudfront create-invalidation \
  --distribution-id $CLOUDFRONT_DIST_ID \
  --paths '/*'
