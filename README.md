> Web client frontend for the OEH photopoints image repository.

## Quickstart

  1. first, install the dependencies

        yarn

  1. optionally (when running locally) define the Google Maps API key to use

        export OEH_GOOGLE_MAPS_API_KEY=AIzaSyBk7IYWflFAAAjJLHHoXvvvw3nJEbFY6tw # replace with your key

  1. either start the webapp using an API that's running locally, or...

        yarn start

  1. ...or, start the webapp using an API that's on the staging server. This method is preferred if you don't want to run the API locally

        export OEH_API_KEY=replace-me # get yourself an API key from AWS API Gateway
        yarn start:staging

  1. open your browser to http://localhost:4200/

## A note about the environment files
We generate the `environment.ts` file so we can inject environment variables. This means if you
open the code before you've run the app at least once, you'll get errors about variables not
existing on the environment object. Just ignore them until you've run the app. If you still have
errors after that, you need to fix them.

## Build for production
```bash
export OEH_API_KEY=replace-me # use the API key to bake into the build
export OEH_GOOGLE_MAPS_API_KEY=replace-me # replace with your key
yarn run build:prod
ls dist/ # all your files are here
```

Big thanks to https://github.com/akveo/ngx-admin for the great starter template. Based on commit [66dff308e61edc56bd37c43d9f1ea447043c2e8d](https://github.com/akveo/ngx-admin/commit/66dff308e61edc56bd37c43d9f1ea447043c2e8d) from 27 Feb 2018.
