import { Component, OnDestroy } from '@angular/core'
import {
  NbMediaBreakpoint,
  NbMediaBreakpointsService,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme'

import { delay, withLatestFrom, takeWhile } from 'rxjs/operators'

@Component({
  selector: 'ngx-layout',
  styleUrls: ['./layout.scss'],
  templateUrl: './layout.html',
})
export class LayoutComponent  implements OnDestroy {

  private alive = true

  constructor(protected menuService: NbMenuService,
              protected themeService: NbThemeService,
              protected bpService: NbMediaBreakpointsService,
              protected sidebarService: NbSidebarService) {

    const isBp = this.bpService.getByName('is')
    this.menuService.onItemSelect()
      .pipe(
        takeWhile(() => this.alive),
        withLatestFrom(this.themeService.onMediaQueryChange()),
        delay(20),
      )
      .subscribe(([item, [bpFrom, bpTo]]: [any, [NbMediaBreakpoint, NbMediaBreakpoint]]) => {

        if (bpTo.width <= isBp.width) {
          this.sidebarService.collapse('menu-sidebar')
        }
      })
  }

  ngOnDestroy() {
    this.alive = false
  }
}
