import { Component, Input, OnInit } from '@angular/core'

import { LinkableIdComponent } from '../../pages/common'

@Component({
  selector: 'ngx-oeh-table',
  templateUrl: './oeh-table.component.html',
  styleUrls: ['./oeh-table.component.scss'],
})
export class OehTableComponent implements OnInit {

  @Input() modelPlural: string
  @Input() settings: any[]
  @Input() service: any // actually a CommonService
  @Input() isDisableCreate? = false

  rowData: any[]
  gridApi: any
  columnApi: any
  columnDefs: any[]

  ngOnInit () {
    this.columnDefs = this.settings.reduce((accum, curr: any) => {
      const def = {
        colId: curr.field,
        headerName: curr.title,
        filterParams: { suppressAndOrCondition: true },
      }
      const typeHandler = getTypeHandler(curr.type)
      const updatedDef = typeHandler(def, curr)
      accum.push(updatedDef)
      return accum
    }, [])
  }

  onGridReady(params) {
    this.gridApi = params.api
    this.columnApi = params.columnApi
    const datasource = {
      getRows: getRowsParams => {
        const start = getRowsParams.startRow
        const limit = getRowsParams.endRow - start
        const sortClause = getRowsParams.sortModel.reduce((accum, curr) => {
          accum.push({
            [curr.colId]: curr.sort.toUpperCase(),
          })
          return accum
        }, [])
        const whereClauseConfig = buildWhereClause(getRowsParams.filterModel)
        const queryParams = Object.assign(
          whereClauseConfig.keyPairs,
          {
            limit: limit,
            skip: start,
            sort: JSON.stringify(sortClause),
          },
        )
        if (whereClauseConfig.complexContraints) {
          queryParams['where'] = whereClauseConfig.complexContraints
        }
        this.service.getAll(queryParams).subscribe(data => {
          const stillMoreRows = -1
          let lastRow = stillMoreRows
          if (data.length < limit) {
            lastRow = start + data.length
          }
          getRowsParams.successCallback(data, lastRow)
          params.api.sizeColumnsToFit()
        })
      },
    }
    this.gridApi.setDatasource(datasource)
  }
}

function coalesceNameGetter (fieldName) {
  return function (params) {
    const record = params.data && params.data[fieldName]
    if (!record) {
      return null
    }
    return record.name
  }
}

function buildWhereClause (filterModel) {
  const result = Object.keys(filterModel).reduce((accum, currKey) => {
    // FIXME handle AND/OR conditions from the UI
    const currAgFilter = filterModel[currKey]
    const value = currAgFilter.filter
    const agGridToSailsBasicMapping = {
      notEqual: '!=',
      startsWith: 'startsWith',
      endsWith: 'endsWith',
      contains: 'contains',
      // notContains is not supported
      lessThan: '<',
      lessThanOrEqual: '<=',
      greaterThan: '>',
      greaterThanOrEqual: '>=',
    }
    const agGridCriteria = currAgFilter.type
    const simpleSailsCriteria = agGridToSailsBasicMapping[agGridCriteria]
    if (simpleSailsCriteria) {
      accum.complexContraints[currKey] = {[simpleSailsCriteria]: value}
      return accum
    }
    const agGridToSailsComplexMapping = {
      equals: () => {
        accum.keyPairs[currKey] = value
      },
      inRange: () => {
        const filterTo = currAgFilter.filterTo
        accum.complexContraints[currKey] = {
          '>=': value,
          '<=': filterTo,
        }
      },
    }
    const complexHandler = agGridToSailsComplexMapping[agGridCriteria]
    if (!complexHandler) {
      throw new Error(`Programmer problem: unsupported criteria encountered '${agGridCriteria}'`)
    }
    complexHandler()
    return accum
  }, {keyPairs: {}, complexContraints: {}})
  result.complexContraints = Object.keys(result.complexContraints).length ? JSON.stringify(result.complexContraints) : undefined
  return result
}

const textFilterOptions = ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith'] // can't support notContains
const handlers = {
  id: (partialResult, config) => {
    partialResult.filterParams = {
      filterOptions: textFilterOptions,
    }
    partialResult.cellRendererFramework = LinkableIdComponent
    return partialResult
  },
  text: (partialResult, config) => {
    partialResult.filterParams = {
      filterOptions: textFilterOptions,
    }
    partialResult.valueFormatter = params => {
      if (!params.value) {
        return '-'
      }
      return params.value
    }
    partialResult.field = config.field
    return partialResult
  },
  vocabText: (partialResult, config) => {
    // we can't allow filtering without a "set" filter because we can't map a
    // partial filter string back to "code" in the DB. ag-grid enterprise
    // offers a set filter but we can't afford it. Might have to DIY one:
    //   https://www.ag-grid.com/javascript-grid-filter-component/
    partialResult.suppressFilter = true
    partialResult.valueFormatter = params => {
      const rawValue = params.data && params.data[config.field] || null
      if (!rawValue) {
        return '-'
      }
      const matchingVocab = config.vocab.find(e => rawValue === e.abbreviation)
      if (!matchingVocab) {
        return rawValue
      }
      return matchingVocab.name
    }
    return partialResult
  },
  number: (partialResult, config) => {
    partialResult.field = config.field
    partialResult.filter = 'agNumberColumnFilter'
    if (config.isCount) {
      partialResult.valueGetter = params => {
        const items = params.data && params.data[config.field] || []
        return items.length
      }
      partialResult.suppressSorting = true
      partialResult.suppressFilter = true
    }
    return partialResult
  },
  date: (partialResult, config) => {
    partialResult.valueGetter = params => {
      const millis = params.data && params.data[config.field] || null
      if (!millis) {
        return null
      }
      return new Date(millis)
    }
    partialResult.valueFormatter = params => {
      if (!params.value) {
        return '-'
      }
      return params.value.toLocaleDateString()
    }
    partialResult.filter = 'agDateColumnFilter'
    return partialResult
  },
  boolean: (partialResult, config) => {
    partialResult.valueGetter = params => {
      if (!params.data) {
        return null
      }
      const value = params.data[config.field]
      if (value === false) {
        return 'no'
      }
      return 'yes'
    }
    partialResult.suppressFilter = true
    return partialResult
  },
  link: (partialResult, config) => {
    partialResult.valueGetter = coalesceNameGetter(config.field)
    partialResult.valueFormatter = params => params.value === null || typeof(params.value) === 'undefined' ? '-' : params.value
    partialResult.suppressSorting = true
    partialResult.suppressFilter = true
    return partialResult
  },
}

function getTypeHandler (type) {
  const result = handlers[type]
  if (!result) {
    throw new Error(`Programmer problem: unsupported column type '${type}'`)
  }
  return result
}
