import { Component, Input, OnInit } from '@angular/core'

export class OehMapMarker {
  constructor(
    lat: number,
    long: number,
    label: string,
  ) {}
}

@Component({
  selector: 'ngx-oeh-map',
  templateUrl: './oeh-map.component.html',
  styles: [`
    #the-map {
      width: 100%;
      height: 400px;
    }
  `],
})
export class OehMapComponent implements OnInit {

  @Input() lat: number
  @Input() long: number
  @Input() markers?: OehMapMarker[]
  @Input() secondaryMarkers?: OehMapMarker[]

  constructor() {}

  ngOnInit() {
    this.validate()
  }

  private validate() {
    if (this.long < -180 || this.long > 180) {
      throw new Error(`The value for long='${this.long}' is outside -180<long<180`)
    }
    if (this.lat < -90 || this.lat > 90) {
      throw new Error(`The value for lat='${this.lat}' is outside -90<lat<90`)
    }
  }
}
