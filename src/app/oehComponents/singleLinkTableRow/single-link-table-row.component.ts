import { Component, Input, OnInit } from '@angular/core'

import * as pathFragments from '../../pages/path-fragments'

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tr[oehSingleLink]', // tslint wants components to be elements but we *need* the keep the <tr> element
  templateUrl: './single-link-table-row.component.html',
})
export class SingleLinkTableRowComponent implements OnInit {

  @Input() label: string
  @Input() pathFragPropName: string
  @Input() record: object
  @Input() targetField: string
  @Input() nameValueField?: string = 'name'
  linkUrl: string
  isValuePresent: boolean = false
  idValue: string
  nameValue: string

  constructor() {}

  ngOnInit() {
    this.validate()
    this.linkUrl = pathFragments[this.pathFragPropName]
    this.isValuePresent = !!this.record[this.targetField]
    if (!this.isValuePresent) {
      return
    }
    this.idValue = this.record[this.targetField].id
    this.nameValue = this.record[this.targetField][this.nameValueField]
  }

  private validate() {
    if (!this.label || this.label.length === 0) {
      throw new Error(`The title attribute must have a value but it was [${this.label}]`)
    }
    if (!this.pathFragPropName || this.pathFragPropName.length === 0) {
      throw new Error(`The pathFragPropName attribute must have a value but it was [${this.pathFragPropName}]`)
    }
    if (!this.targetField || this.targetField.length === 0) {
      throw new Error(`The targetField attribute must have a value but it was [${this.targetField}]`)
    }
  }
}
