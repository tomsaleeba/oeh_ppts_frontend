import { Component, Input, OnInit } from '@angular/core'

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tr[oehSingle]', // tslint wants components to be elements but we *need* the keep the <tr> element
  templateUrl: './single-table-row.component.html',
})
export class SingleTableRowComponent implements OnInit {

  @Input() label: string
  @Input() record: object
  @Input() targetField: string
  isValuePresent: boolean = false
  value: string

  constructor() {}

  ngOnInit() {
    this.validate()
    const value = this.record[this.targetField]
    this.isValuePresent = !!value || value === 0
    if (!this.isValuePresent) {
      return
    }
    this.value = value
  }

  private validate() {
    if (!this.label || this.label.length === 0) {
      throw new Error(`The title attribute must have a value but it was [${this.label}]`)
    }
    if (!this.targetField || this.targetField.length === 0) {
      throw new Error(`The targetField attribute must have a value but it was [${this.targetField}]`)
    }
  }
}
