import { Component, OnInit } from '@angular/core'

import { FieldType } from '@ngx-formly/core'

export const idFieldName = 'itemId'
export const labelFieldName = 'itemLabel'

@Component({
  selector: 'ngx-oeh-multi-select',
  templateUrl: './oeh-multi-select.component.html',
  // let's try to fit in with the Bootstrap styles
  // FIXME using /deep/ *AND* !important... so dirty :'(
  styles: [`
    /deep/ .multiselect-dropdown .dropdown-btn {
      padding: 1em !important;
      border: 2px solid #dadfe6 !important;
      border-radius: 5px !important;
    }
  `],
})
export class OehMultiSelectComponent extends FieldType implements OnInit {
  dropdownList = []
  selectedItems = []
  dropdownSettings = {}

  ngOnInit() {
    this.populateItems()
    this.dropdownSettings = {
      singleSelection: false,
      idField: idFieldName,
      textField: labelFieldName,
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
    }
    const origReset = this.options.resetModel
    this.options.resetModel = (resetModel?: any) => {
      if (resetModel && resetModel[this.key]) {
        this.selectedItems = resetModel[this.key]
      } else {
        this.selectedItems = []
      }
      origReset(resetModel)
    }
  }

  private populateItems() {
    if (!this.to.items) {
      return
    }
    this.to.items.subscribe(items => {
      this.dropdownList = items
      const isPreselections = this.model[this.key]
      if (!isPreselections) {
        return
      }
      this.selectedItems = mapPreselections(this.model[this.key], items)
      this.updateModel()
    })
  }

  updateModel() {
    this.model[this.key] = this.selectedItems.reduce(extractIds, [])
  }

  updateModelAll(items: any) {
    this.model[this.key] = items.reduce(extractIds, [])
  }
}

function extractIds (accum, curr) {
  accum.push(curr[idFieldName])
  return accum
}

function mapPreselections (preselections: any[], items: any[]) {
  const labelMapping = items.reduce((accum, curr) => {
    const id = curr[idFieldName]
    const label = curr[labelFieldName]
    accum[id] = label
    return accum
  }, {})
  const result = preselections.reduce((accum, curr) => {
    let id = curr
    if (typeof(curr) === 'object') {
      id = curr.id
    }
    let label = labelMapping[id]
    if (!label) {
      console.error(`Data problem: could not find a label for preselection with ID='${id}', ` +
        `available items='${JSON.stringify(items, null, 2)}'`)
      label = `(no label for ${id})`
    }
    accum.push({
      [idFieldName]: id,
      [labelFieldName]: label,
    })
    return accum
  }, [])
  return result
}
