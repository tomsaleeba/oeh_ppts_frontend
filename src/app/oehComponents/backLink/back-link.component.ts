import { Component, OnInit, Input } from '@angular/core'

@Component({
  selector: 'ngx-back-link',
  templateUrl: 'back-link.component.html',
})

export class BackLinkComponent implements OnInit {

  @Input() modelPlural: string
  @Input() crumbLabel: string
  typeFragment: string

  ngOnInit() {
    this.typeFragment = this.modelPlural
  }
}
