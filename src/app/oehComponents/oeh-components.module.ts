import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'

import { ThemeModule } from '../@theme/theme.module'
import { AgGridModule } from 'ag-grid-angular'
import { AgmCoreModule } from '@agm/core'
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown'

import { environment } from '../../environments/environment'
import { BackLinkComponent } from './backLink/back-link.component'
import { EditLinkComponent } from './editLink/edit-link.component'
import { MultiLinkTableRowComponent } from './multiLinkTableRow/multi-link-table-row.component'
import { OehMapComponent } from './oehMap/oeh-map.component'
import { SingleLinkTableRowComponent } from './singleLinkTableRow/single-link-table-row.component'
import { SingleTableRowComponent } from './singleTableRow/single-table-row.component'
import { OehTableComponent } from './oehTable/oeh-table.component'
import { OehMultiSelectComponent } from './oehMultiSelect/oeh-multi-select.component'
import { LinkableIdComponent } from '../pages/common'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ThemeModule,
    AgGridModule.withComponents([
      LinkableIdComponent,
    ]),

    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsApiKey,
    }),
    AgmJsMarkerClustererModule,
    NgMultiSelectDropDownModule.forRoot(),
  ],
  declarations: [
    BackLinkComponent,
    EditLinkComponent,
    MultiLinkTableRowComponent,
    OehMapComponent,
    SingleLinkTableRowComponent,
    SingleTableRowComponent,
    OehTableComponent,
    OehMultiSelectComponent,
    LinkableIdComponent,
  ],
  exports: [
    BackLinkComponent,
    EditLinkComponent,
    MultiLinkTableRowComponent,
    OehMapComponent,
    SingleLinkTableRowComponent,
    SingleTableRowComponent,
    OehTableComponent,
    OehMultiSelectComponent,
  ],
  entryComponents: [
    LinkableIdComponent,
  ],
})
export class OehComponentsModule {}
