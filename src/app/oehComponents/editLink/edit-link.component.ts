import { Component, Input } from '@angular/core'

@Component({
  selector: 'ngx-edit-link',
  templateUrl: 'edit-link.component.html',
})

export class EditLinkComponent {
  @Input() targetRecordId: string
}
