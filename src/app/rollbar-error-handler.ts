import { environment } from '../environments/environment'
import * as Rollbar from 'rollbar'
import {
  Injectable,
  Injector,
  InjectionToken,
  ErrorHandler,
} from '@angular/core'

const rollbarConfig = {
  accessToken: 'b00303eb94ba49539022e62c1a1a7c00',
  environment: environment.rollbarEnv,
  captureUncaught: true,
  captureUnhandledRejections: true,
}

export const RollbarService = new InjectionToken<Rollbar>('rollbar')

@Injectable()
export class RollbarErrorHandler implements ErrorHandler {
  constructor(private injector: Injector) {}

  handleError(err: any): void {
    if (environment.production) {
      const rollbar = this.injector.get(RollbarService)
      rollbar.error(err.originalError || err)
    }
    console.error(err.originalError || err)
  }
}

export function rollbarFactory() {
    return new Rollbar(rollbarConfig)
}

