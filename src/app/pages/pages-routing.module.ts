import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'

import { PagesComponent } from './pages.component'
import * as pf from './path-fragments'
import { DashboardComponent } from './dashboard/dashboard.component'
import { PageNotFoundComponent } from './pageNotFound/page-not-found.component'
import { ErrorMessageComponent } from './errorMessage/error-message.component'

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: pf.dashboard,
        component: DashboardComponent,
      }, {
        path: pf.notFound,
        component: PageNotFoundComponent,
      }, {
        path: pf.errorMessage,
        component: ErrorMessageComponent,
      }, {
        path: pf.census,
        loadChildren: './census/census.module#CensusModule',
      }, {
        path: pf.organisation,
        loadChildren: './organisation/organisation.module#OrganisationModule',
      }, {
        path: pf.photoPanorama,
        loadChildren: './photo-panorama/photo-panorama.module#PhotoPanoramaModule',
      }, {
        path: pf.photoUpDown,
        loadChildren: './photo-up-down/photo-up-down.module#PhotoUpDownModule',
      }, {
        path: pf.plot,
        loadChildren: './plot/plot.module#PlotModule',
      }, {
        path: pf.qr,
        loadChildren: './qr/qr.module#QrModule',
      }, {
        path: pf.studyArea,
        loadChildren: './studyarea/studyarea.module#StudyAreaModule',
      }, {
        path: pf.photopointUser,
        loadChildren: './photopoint-user/photopoint-user.module#PhotopointUserModule',
      }, {
        path: '',
        redirectTo: pf.dashboard,
        pathMatch: 'full',
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
