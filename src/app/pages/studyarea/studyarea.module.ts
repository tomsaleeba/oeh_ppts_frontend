import { NgModule } from '@angular/core'

import { OehComponentsModule } from '../../oehComponents'
import { FormComponentsModule } from '../../formComponents'
import { ThemeModule } from '../../@theme/theme.module'
import { StudyAreaRoutingModule } from './studyarea-routing.module'
import { StudyAreaComponent } from './studyarea.component'
import { StudyAreaAllComponent } from './all/studyarea-all.component'
import { StudyAreaSingleComponent } from './single/studyarea-single.component'
import { StudyAreaService } from './studyarea.service'
import { StudyAreaDataResolver } from './single/studyarea-single-resolver.service'
import { StudyAreaNameListResolver } from './resolvers/studyarea-name-list-resolver.service'
import { StudyAreaCreateComponent } from './create/studyarea-create.component'

@NgModule({
  imports: [
    ThemeModule,
    StudyAreaRoutingModule,
    OehComponentsModule,
    FormComponentsModule,
  ],
  declarations: [
    StudyAreaComponent,
    StudyAreaAllComponent,
    StudyAreaSingleComponent,
    StudyAreaCreateComponent,
  ],
  providers: [
    StudyAreaService,
    StudyAreaDataResolver,
    StudyAreaNameListResolver,
  ],
})
export class StudyAreaModule { }
