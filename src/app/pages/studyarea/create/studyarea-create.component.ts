import { Component, ErrorHandler } from '@angular/core'
import { Location } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router'
import { plainTextField, selectField, multiSelectField, textAreaField } from '../../../formComponents'

import { StudyAreaService } from '../studyarea.service'
import { fullStudyAreaSingle } from '../../path-fragments'
import { CommonCreateComponent } from '../../../formComponents'

@Component({
  templateUrl: '../../../formComponents/commonCreate/common-create.component.html',
})
export class StudyAreaCreateComponent extends CommonCreateComponent {

  orgNameList: [{}]

  constructor(
    readonly location: Location,
    readonly route: ActivatedRoute,
    readonly router: Router,
    readonly service: StudyAreaService,
    readonly errorHandler: ErrorHandler,
  ) {
    super(location, route, router, service, fullStudyAreaSingle, 'Study area',
      errorHandler, StudyAreaCreateComponent.getFields(route))
  }

  static getFields(route) {
    const result = []
    route.data.subscribe(data => {
      const isEditMode = data.isEditMode
      result.push(
        plainTextField('name', 'Study area name', isEditMode, {isRequired: true, maxLength: 200}),
        textAreaField('description', 'Description', isEditMode, {maxLength: 999999}),
        plainTextField('program', 'Program name', isEditMode, {isRequired: true, maxLength: 200}),
        selectField('coordinator', 'Coordinator', route.data, 'userList'),
        // not handling 'boundary' field at this stage
        multiSelectField('plots', 'Plots', route.data, 'plotList'),
      )
    })
    return result
  }

  prepModelForEdit() {
    if (this.model.coordinator) {
      this.model.coordinator = this.model.coordinator.id
    }
  }

}
