import { Component } from '@angular/core'

import { StudyAreaService } from '../studyarea.service'

@Component({
  templateUrl: './studyarea-all.component.html',
})
export class StudyAreaAllComponent {

  constructor(
    public studyAreaService: StudyAreaService,
  ) {}

  settings = [
    {
      field: 'name',
      title: 'Name',
      type: 'id',
    }, {
      field: 'description',
      title: 'Description',
      type: 'text',
    }, {
      field: 'program',
      title: 'Program',
      type: 'text',
    }, {
      field: 'coordinator',
      title: 'Coordinator',
      type: 'link',
    }, {
      field: 'plots',
      title: 'Plot count',
      type: 'number',
      isCount: true,
    },
  ]
}
