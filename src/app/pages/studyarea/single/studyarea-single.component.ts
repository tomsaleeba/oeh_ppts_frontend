import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { StudyArea } from '../studyarea.service'

@Component({
  templateUrl: './studyarea-single.component.html',
  styles: [`
    table tbody tr th {
      width: 15em
    }
  `],
})
export class StudyAreaSingleComponent implements OnInit {

  saData: any

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: {studyArea: StudyArea}) => {
      this.saData = data.studyArea
    })
  }
}
