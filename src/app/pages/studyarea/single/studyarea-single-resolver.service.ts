import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { CommonDataResolver } from '../../common'
import { StudyAreaService, StudyArea } from '../studyarea.service'

@Injectable()
export class StudyAreaDataResolver extends CommonDataResolver<StudyArea> {
  constructor(service: StudyAreaService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
