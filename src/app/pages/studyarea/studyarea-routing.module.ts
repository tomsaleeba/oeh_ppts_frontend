import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { PlotNameListResolver } from '../plot/resolvers/plot-name-list-resolver.service'
import { PhotopointUserNameListResolver } from '../photopoint-user/resolvers/photopoint-user-name-list-resolver.service'

import { StudyAreaComponent } from './studyarea.component'
import { StudyAreaAllComponent } from './all/studyarea-all.component'
import { StudyAreaSingleComponent } from './single/studyarea-single.component'
import { StudyAreaDataResolver } from './single/studyarea-single-resolver.service'
import { StudyAreaCreateComponent } from './create/studyarea-create.component'
import { all, single, create, edit } from '../path-fragments'

const routes: Routes = [{
  path: '',
  component: StudyAreaComponent,
  children: [{
    path: all,
    component: StudyAreaAllComponent,
  }, {
    path: single,
    component: StudyAreaSingleComponent,
    resolve: {
      studyArea: StudyAreaDataResolver,
    },
  }, {
    path: create,
    component: StudyAreaCreateComponent,
    resolve: {
      plotList: PlotNameListResolver,
      userList: PhotopointUserNameListResolver,
    },
  }, {
    path: edit,
    component: StudyAreaCreateComponent,
    data: {
      isEditMode: true,
    },
    resolve: {
      existing: StudyAreaDataResolver,
      plotList: PlotNameListResolver,
      userList: PhotopointUserNameListResolver,
    },
  }],
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudyAreaRoutingModule {
}
