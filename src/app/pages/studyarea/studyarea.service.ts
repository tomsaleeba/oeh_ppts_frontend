import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { CommonService, CommonModel } from '../common/common.service'
import { fullStudyAreaSingle } from '../path-fragments'

@Injectable()
export class StudyAreaService extends CommonService<StudyArea> {

  constructor(
    http: HttpClient,
  ) {
    super(http, 'studyarea', fullStudyAreaSingle)
  }
}

export class StudyArea extends CommonModel {
  constructor (
    readonly name: string,
    readonly description: string,
    readonly program: string,
    readonly coordinator, // TODO add User model
    readonly boundary: {}, // TODO add more type information for boundary
  ) {
    super()
  }
}
