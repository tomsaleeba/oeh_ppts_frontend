import { TestBed, inject } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { StudyAreaService } from './studyarea.service'
import { environment } from '../../../environments/environment'

describe('StudyAreaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [StudyAreaService],
    })
  })

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify()
  })))

  it('should be created', inject([StudyAreaService], (service: StudyAreaService) => {
    expect(service).toBeTruthy()
  }))

  it('should return all study areas', inject([StudyAreaService, HttpTestingController],
      (service: StudyAreaService, httpMock: HttpTestingController) => {
    service.getAll().subscribe((result) => {
      expect(result.length).toBe(2)
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/studyarea`)
    expect(req.request.method).toBe('GET')
    req.flush([{id: 1}, {id: 2}])
  }))
})
