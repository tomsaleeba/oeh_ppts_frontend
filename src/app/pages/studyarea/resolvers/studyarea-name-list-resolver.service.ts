import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { StudyAreaService } from '../studyarea.service'
import { CommonNameListResolver } from '../../common/common-name-list-resolver.service'

@Injectable()
export class StudyAreaNameListResolver extends CommonNameListResolver {
  constructor(service: StudyAreaService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
