import { NgModule } from '@angular/core'

import { OehComponentsModule } from '../../oehComponents'
import { FormComponentsModule } from '../../formComponents'
import { ThemeModule } from '../../@theme/theme.module'
import { PhotoPanoramaRoutingModule } from './photo-panorama-routing.module'
import { PhotoPanoramaComponent } from './photo-panorama.component'
import { PhotoPanoramaAllComponent } from './all/photo-panorama-all.component'
import { PhotoPanoramaSingleComponent } from './single/photo-panorama-single.component'
import { PhotoPanoramaService } from './photo-panorama.service'
import { PhotoPanoramaDataResolver } from './single/photo-panorama-single-resolver.service'
import { PhotoPanoramaBulkComponent } from './bulk/photo-panorama-bulk.component'
import { IsWebSafeResolver } from './single/is-web-safe-resolver.service'
import { PhotoPanoramaEditComponent } from './edit/photo-panorama-edit.component'

@NgModule({
  imports: [
    ThemeModule,
    PhotoPanoramaRoutingModule,
    OehComponentsModule,
    FormComponentsModule,
  ],
  declarations: [
    PhotoPanoramaComponent,
    PhotoPanoramaAllComponent,
    PhotoPanoramaSingleComponent,
    PhotoPanoramaBulkComponent,
    PhotoPanoramaEditComponent,
  ],
  providers: [
    PhotoPanoramaService,
    PhotoPanoramaDataResolver,
    IsWebSafeResolver,
  ],
})
export class PhotoPanoramaModule { }
