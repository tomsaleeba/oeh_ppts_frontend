import { Injectable, ErrorHandler } from '@angular/core'
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { of } from 'rxjs'
import { take, catchError } from 'rxjs/operators'

import { environment } from '../../../../environments/environment'
import { fullErrorMessage } from '../../path-fragments'

@Injectable()
export class IsWebSafeResolver implements Resolve<Boolean> {
  constructor(
    private http: HttpClient,
    private router: Router,
    private errorHandler: ErrorHandler,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const panoramaId = route.queryParamMap.get('id')
    const url = `${environment.apiUrl}/photopanorama/is-web-safe`
    const opts = {
      params: {
        panoramaId: panoramaId,
      },
      headers: {
        'x-api-key': environment.apiKey,
      },
    }
    return this.http.get(url, opts).pipe(
      catchError((err, caught) => {
        const msg = `Failed when checking if panorama with id='${panoramaId}' is safe for web display, with ` +
          `error=${JSON.stringify(err, null, 2)}`
        this.errorHandler.handleError(new Error(msg))
        this.router.navigate([fullErrorMessage])
        return of([] as any)
      }),
      take(1),
    )
  }
}


