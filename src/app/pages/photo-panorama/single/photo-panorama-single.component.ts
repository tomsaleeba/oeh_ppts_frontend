import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'


@Component({
  templateUrl: './photo-panorama-single.component.html',
  styles: [`
    table tbody tr th {
      width: 15em
    }
  `],
})
export class PhotoPanoramaSingleComponent implements OnInit {

  photoPanoramaData: any
  thumbnailBaseUrl: string
  rawDownloadUrl: string
  isPhotoWebSafe: boolean

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.photoPanoramaData = data.photoPanorama
      this.thumbnailBaseUrl = data.thumbnailBaseUrl
      this.isPhotoWebSafe = data.isWebSafe.isWebSafe
      this.rawDownloadUrl = data.isWebSafe.rawUrl
    })
  }

  isEditable () {
    return this.photoPanoramaData.source === 'dslr' // TODO perhaps should be looked up in vocab?
  }

  getPhotoUrlMediumSize () {
    return `${this.thumbnailBaseUrl}/fit-in/900x0/${this.photoPanoramaData.data}`
  }

  getPhotoUrlFullSize () {
    return `${this.thumbnailBaseUrl}/${this.photoPanoramaData.data}`
  }

  getIsStitched () {
    return this.photoPanoramaData.isStitched ? 'yes' : 'no'
  }
}
