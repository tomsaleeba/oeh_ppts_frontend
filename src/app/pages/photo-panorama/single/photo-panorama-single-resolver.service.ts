import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { PhotoPanoramaService, PhotoPanorama } from '../photo-panorama.service'
import { CommonDataResolver } from '../../common'

@Injectable()
export class PhotoPanoramaDataResolver extends CommonDataResolver<PhotoPanorama> {
  constructor(service: PhotoPanoramaService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
