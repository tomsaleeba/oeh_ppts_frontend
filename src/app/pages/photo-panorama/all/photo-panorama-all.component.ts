import { Component } from '@angular/core'

import { PhotoPanoramaService } from '../photo-panorama.service'

@Component({
  templateUrl: './photo-panorama-all.component.html',
})
export class PhotoPanoramaAllComponent {

  constructor(
    public photoPanoramaService: PhotoPanoramaService,
  ) {}

  settings = [
    {
      field: 'name',
      title: 'Name',
      type: 'id',
    }, {
      field: 'point',
      title: 'Point #',
      type: 'text',
    }, {
      field: 'sequence',
      title: 'Sequence',
      type: 'number',
    }, {
      field: 'source',
      title: 'Source',
      type: 'text',
    }, {
      field: 'isStitched',
      title: 'Stitched?',
      type: 'boolean',
    }, {
      field: 'census',
      title: 'Census',
      type: 'link',
    },
  ]
}
