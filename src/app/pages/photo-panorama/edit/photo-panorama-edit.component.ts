import { Component, ErrorHandler } from '@angular/core'
import { Location } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router'
import { selectField, numberField } from '../../../formComponents'

import { PhotoPanoramaService } from '../photo-panorama.service'
import { fullPhotoPanoramaSingle } from '../../path-fragments'
import { CommonCreateComponent } from '../../../formComponents'
import { pointNumberRadio } from '../bulk/pointnumber-radio'

@Component({
  templateUrl: '../../../formComponents/commonCreate/common-create.component.html',
})
export class PhotoPanoramaEditComponent extends CommonCreateComponent {

  constructor(
    readonly location: Location,
    readonly route: ActivatedRoute,
    readonly router: Router,
    readonly service: PhotoPanoramaService,
    readonly errorHandler: ErrorHandler,
  ) {
    super(location, route, router, service, fullPhotoPanoramaSingle, 'Panorama Photo',
      errorHandler, PhotoPanoramaEditComponent.getFields(route))
  }

  static getFields(route) {
    const result = []
    route.data.subscribe(data => {
      result.push(
        selectField('census', 'Census', route.data, 'censusList'),
        pointNumberRadio,
        numberField('sequence', 'Sequence number', true, {
          isRequired: true,
          min: 0,
          max: 999,
        }),
      )
    })
    return result
  }

  prepModelForEdit() {
    if (this.model.census) {
      this.model.census = this.model.census.id
    }
  }

}
