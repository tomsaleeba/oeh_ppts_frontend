import { Component, OnInit, NgZone } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ErrorHandler } from '@angular/core'
import { catchError } from 'rxjs/operators'
import { of } from 'rxjs'

import * as Uppy from 'uppy'
import { FormGroup } from '@angular/forms'
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core'
import * as exifParser from 'exif-parser'
import * as asyncReduce from 'async-array-reduce'
import * as isJpg from 'is-jpg'

import { selectField } from '../../../formComponents'
import { PhotoPanoramaService } from '../photo-panorama.service'
import { fullCensusSingle } from '../../path-fragments'
import { pointNumberRadio } from './pointnumber-radio'

const nameAlphaSort = (a, b) => {
  const nameA = a.name.toUpperCase()
  const nameB = b.name.toUpperCase()
  if (nameA < nameB) {
    return -1
  }
  if (nameA > nameB) {
    return 1
  }
  return 0
}

enum States {
  initial,
  postProcessing,
  donePostProcessing,
  errorValidation,
  errorFileUpload,
  errorNoSuccessfulResults,
  errorNoUploadUrl,
  errorReduce,
  errorBulkPost,
}

@Component({
  templateUrl: './photo-panorama-bulk.component.html',
})
export class PhotoPanoramaBulkComponent implements OnInit {

  model: any = {}
  fields: FormlyFieldConfig[]
  form = new FormGroup({})
  options: FormlyFormOptions = {}
  state = States.initial
  states = States
  private tusEndpoint: String
  censusSingleUrl = fullCensusSingle

  constructor(
    private route: ActivatedRoute,
    private service: PhotoPanoramaService,
    private errorHandler: ErrorHandler,
    private ngZone: NgZone,
  ) {
    this.fields = [
      selectField('census', 'Census', this.route.data, 'censusList', {isRequired: true}),
      pointNumberRadio,
    ]
  }

  ngOnInit() {
    this.form.statusChanges.subscribe(() => {
      this.setState(States.initial)
    })
    this.route.data.subscribe(data => {
      this.tusEndpoint = data.tusEndpoint
      this.initUppy()
    })
  }

  private initUppy () {
    const uppy = Uppy.Core({
      debug: false,
      autoProceed: false,
      restrictions: {
        maxFileSize: 50 * 1000 * 1000,
        minNumberOfFiles: 0,
        maxNumberOfFiles: 1000,
      },
      onBeforeUpload: () => {
        return this.ngZone.run(() => {
          this.setState(States.initial)
          if (!this.form.valid) {
            this.setState(States.errorValidation)
            return false
          }
          this.options.formState.disabled = true
          return true
        })
      },
    } as any)
    .use(Uppy.Dashboard, {
      inline: true,
      target: '#uppy',
      replaceTargetContent: true,
      showProgressDetails: true,
      note: 'Images only, up to 30 MB per file',
      height: 470,
      browserBackButtonClose: true,
    } as any)
    .use(Uppy.Tus, {
      endpoint: `${this.tusEndpoint}`,
    })

    uppy.on('upload-error', (file, err) => {
      this.setState(States.errorFileUpload)
      const msg = `Panorama upload failed for file='${file.name}', with error=${JSON.stringify(err, null, 2)}`
      this.errorHandler.handleError(new Error(msg))
    })

    uppy.on('complete', result => {
      this.options.formState.disabled = false
      this.setState(States.postProcessing)
      if (result.successful.length === 0) {
        this.setState(States.errorNoSuccessfulResults)
        return
      }
      result.successful.sort(nameAlphaSort)
      let currSeq = 1
      for (const curr of result.successful) {
        (curr as any).sequence = currSeq++
        if (!curr.uploadURL) {
          const msg = `[Bulk uploader] encountered a "success" file without an uploadURL, this should never happen (but it does). ` +
            ` file='${JSON.stringify(curr, null, 2)}'`
          this.errorHandler.handleError(new Error(msg))
          this.setState(States.errorNoUploadUrl)
          return
        }
      }
      const reducerFn = (accum, curr, next) => {
        const reader = new FileReader()
        reader.onload = (e) => {
          const imageBytes = (e.target as any).result
          const exif = this.extractExif(imageBytes, curr)
          accum.push({
            datastoreId: curr.uploadURL.replace(/.*\//, ''),
            census: this.model.census,
            point: this.model.point,
            sequence: curr.sequence,
            exif: exif,
          })
          return next(null, accum)
        }
        reader.readAsArrayBuffer(curr.data)
      }
      asyncReduce(result.successful, [], reducerFn, (reduceErr, updates) => {
        if (reduceErr) {
          this.errorHandler.handleError(reduceErr)
          this.setState(States.errorReduce)
        }
        this.service.bulkUpdate(updates as any).pipe(
          catchError((bulkUpdateErr, caught) => {
            this.errorHandler.handleError(bulkUpdateErr)
            this.setState(States.errorBulkPost)
            return of(false)
          }),
        ).subscribe(resp => {
          if (!resp || !(resp as any).success) {
            return
          }
          this.setState(States.donePostProcessing)
        })
      })
    })
  }

  private extractExif (imageBytes, currFile) {
    const noExifForYou = null
    const isNotJpg = !isJpg(new Uint8Array(imageBytes))
    if (isNotJpg) {
      return noExifForYou
    }
    const parser = exifParser.create(imageBytes)
    let exifData
    try {
      exifData = parser.parse()
    } catch (error) {
      this.errorHandler.handleError(`Failed to parse the panorama photo with name='${currFile.name}', ` +
        `MIME='${currFile.type}', error='${JSON.stringify({msg: error.message, stack: error.stack}, null, 2)}'`)
      return noExifForYou
    }
    const tagsToGet = ['Make', 'Model', 'Software', 'ISO', 'FNumber', 'ApertureValue']
    const result = tagsToGet.reduce((accum, curr) => {
      if (exifData.tags && exifData.tags[curr]) {
        accum[curr] = exifData.tags[curr]
      }
      return accum
    }, {})
    const isNoExif = Object.keys(result).length === 0
    if (isNoExif) {
      return noExifForYou
    }
    return result
  }

  private setState(s: States) {
    this.ngZone.run(() => {
      this.state = s
    })
  }

  isInErrorState() {
    const errorStates = [
      States.errorBulkPost,
      States.errorFileUpload,
      States.errorNoSuccessfulResults,
      States.errorNoUploadUrl,
      States.errorReduce,
      States.errorValidation,
    ]
    return errorStates.indexOf(this.state) >= 0
  }

}
