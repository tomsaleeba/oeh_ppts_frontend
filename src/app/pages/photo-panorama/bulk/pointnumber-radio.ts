export const pointNumberRadio = {
  key: 'point',
    type: 'radio',
    templateOptions: {
      label: 'Photopoint point number',
        required: true,
        options: [
          { value: 1, label: 'Point 1' },
          { value: 2, label: 'Point 2' },
          { value: 3, label: 'Point 3' },
        ],
    },
}
