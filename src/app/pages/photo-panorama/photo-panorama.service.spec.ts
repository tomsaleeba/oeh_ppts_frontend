import { TestBed, inject } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { PhotoPanoramaService } from './photo-panorama.service'
import { environment } from '../../../environments/environment'

describe('PhotoPanoramaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PhotoPanoramaService],
    })
  })

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify()
  })))

  it('should be created', inject([PhotoPanoramaService], (service: PhotoPanoramaService) => {
    expect(service).toBeTruthy()
  }))

  it('should return all photoPanoramas', inject([PhotoPanoramaService, HttpTestingController],
      (service: PhotoPanoramaService, httpMock: HttpTestingController) => {
    service.getAll().subscribe((result) => {
      expect(result.length).toBe(2)
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/photopanorama`)
    expect(req.request.method).toBe('GET')
    req.flush([{id: 1}, {id: 2}])
  }))
})
