import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { PhotoPanoramaComponent } from './photo-panorama.component'
import { PhotoPanoramaAllComponent } from './all/photo-panorama-all.component'
import { PhotoPanoramaSingleComponent } from './single/photo-panorama-single.component'
import { PhotoPanoramaDataResolver } from './single/photo-panorama-single-resolver.service'
import { PhotoPanoramaBulkComponent } from './bulk/photo-panorama-bulk.component'
import { PhotoPanoramaEditComponent } from './edit/photo-panorama-edit.component'
import { CensusNameListResolver } from '../census/resolvers/census-name-list-resolver.service'
import { all, single, bulk, edit } from '../path-fragments'
import { TusEndpointResolver } from '../common/clientConfig/tus-endpoint-resolver.service'
import { ThumbnailBaseUrlResolver } from '../common/clientConfig/thumbnail-base-url-resolver.service'
import { IsWebSafeResolver } from './single/is-web-safe-resolver.service'

const routes: Routes = [{
  path: '',
  component: PhotoPanoramaComponent,
  children: [{
    path: all,
    component: PhotoPanoramaAllComponent,
  }, {
    path: single,
    component: PhotoPanoramaSingleComponent,
    resolve: {
      photoPanorama: PhotoPanoramaDataResolver,
      thumbnailBaseUrl: ThumbnailBaseUrlResolver,
      isWebSafe: IsWebSafeResolver,
    },
  }, {
    path: bulk,
    component: PhotoPanoramaBulkComponent,
    resolve: {
      censusList: CensusNameListResolver,
      tusEndpoint: TusEndpointResolver,
    },
  }, {
    path: edit,
    component: PhotoPanoramaEditComponent,
    resolve: {
      existing: PhotoPanoramaDataResolver,
      censusList: CensusNameListResolver,
    },
  }],
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhotoPanoramaRoutingModule {}
