import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders  } from '@angular/common/http'

import { environment } from '../../../environments/environment'
import { CommonService, CommonModel, apiUrl } from '../common/common.service'
import { fullPhotoPanoramaSingle } from '../path-fragments'

const fragment = 'photopanorama'

@Injectable()
export class PhotoPanoramaService extends CommonService<PhotoPanorama> {

  constructor(
    http: HttpClient,
  ) {
    super(http, fragment, fullPhotoPanoramaSingle, 'photop')
  }

  bulkUpdate(updates: [{datastoreId: string, census: string, point: number, sequence: number}]) {
    const url = apiUrl(`${fragment}/bulk`)
    return this.http.post(url, {
      updates: updates,
    }, {
      headers: new HttpHeaders({
        'x-api-key': environment.apiKey,
      }),
    })
  }
}

export class PhotoPanorama extends CommonModel {
  constructor (
    readonly name: string,
    readonly data: string,
    readonly point: number,
    readonly sequence: number,
    readonly source: string,
    readonly isStitched: boolean,
  ) {
    super()
  }
}
