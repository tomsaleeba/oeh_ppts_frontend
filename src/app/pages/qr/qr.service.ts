import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { CommonService } from '../common'
import { CommonModel } from '../common/common.service';
import { fullQrSingle } from '../path-fragments'

@Injectable()
export class QrService extends CommonService<Qr> {

  constructor(
    http: HttpClient,
  ) {
    super(http, 'qr', fullQrSingle)
  }
}

export class Qr extends CommonModel {
  constructor(
    readonly qrUUID: string,
    // TODO add more fields
  ) {
    super()
  }
}
