import { TestBed, inject } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { QrService } from './qr.service'
import { environment } from '../../../environments/environment'

describe('QrService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [QrService],
    })
  })

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify()
  })))

  it('should return all QRs', inject([QrService, HttpTestingController],
      (service: QrService, httpMock: HttpTestingController) => {
    service.getAll().subscribe((result) => {
      expect(result.length).toBe(2)
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/qr`)
    expect(req.request.method).toBe('GET')
    req.flush([{id: 1}, {id: 2}])
  }))
})
