import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { QrComponent } from './qr.component'
import { QrAllComponent } from './all/qr-all.component'
import { QrSingleComponent } from './single/qr-single.component'
import { QrDataResolver } from './single/qr-single-resolver.service'
import { all, single } from '../path-fragments'

const routes: Routes = [{
  path: '',
  component: QrComponent,
  children: [{
    path: all,
    component: QrAllComponent,
  }, {
    path: single,
    component: QrSingleComponent,
    resolve: {
      qr: QrDataResolver,
    },
  }],
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QrRoutingModule {
}
