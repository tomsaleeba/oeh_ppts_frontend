import { NgModule } from '@angular/core'

import { QRCodeModule } from 'angularx-qrcode'

import { OehComponentsModule } from '../../oehComponents'
import { ThemeModule } from '../../@theme/theme.module'
import { QrRoutingModule } from './qr-routing.module'
import { QrComponent } from './qr.component'
import { QrAllComponent } from './all/qr-all.component'
import { QrSingleComponent } from './single/qr-single.component'
import { QrService } from './qr.service'
import { QrDataResolver } from './single/qr-single-resolver.service'

@NgModule({
  imports: [
    ThemeModule,
    QrRoutingModule,
    QRCodeModule,
    OehComponentsModule,
  ],
  declarations: [
    QrComponent,
    QrAllComponent,
    QrSingleComponent,
  ],
  entryComponents: [
    QrSingleComponent,
  ],
  providers: [
    QrService,
    QrDataResolver,
  ],
})
export class QrModule { }
