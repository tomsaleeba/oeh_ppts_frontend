import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { Qr } from '../qr.service'

@Component({
  templateUrl: './qr-single.component.html',
  styles: [`
    table tbody tr th {
      width: 15em
    }
  `],
})
export class QrSingleComponent implements OnInit {

  qrData: any

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: {qr: Qr}) => {
      this.qrData = data.qr
    })
  }
}
