import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { CommonDataResolver } from '../../common'
import { QrService, Qr } from '../qr.service'

@Injectable()
export class QrDataResolver extends CommonDataResolver<Qr> {
  constructor(service: QrService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
