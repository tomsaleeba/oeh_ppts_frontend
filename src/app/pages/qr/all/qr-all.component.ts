import { Component } from '@angular/core'

import { QrService } from '../qr.service'

@Component({
  templateUrl: './qr-all.component.html',
})
export class QrAllComponent {

  constructor(
    public qrService: QrService,
  ) {}

  settings = [
    {
      field: 'qrUUID',
      title: 'QR ID',
      type: 'id',
    }, {
      field: 'plot',
      title: 'Plot',
      type: 'link',
    }, {
      field: 'user',
      title: 'User',
      type: 'link',
    },
  ]
}
