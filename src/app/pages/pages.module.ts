import { NgModule } from '@angular/core'

import { PagesComponent } from './pages.component'
import { DashboardModule } from './dashboard/dashboard.module'
import { PageNotFoundModule } from './pageNotFound/page-not-found.module'
import { ErrorMessageModule } from './errorMessage/error-message.module'
import { CensusModule } from './census'
import { PhotopointUserModule } from './photopoint-user'
import { PhotoPanoramaModule } from './photo-panorama'
import { PhotoUpDownModule } from './photo-up-down'
import { PlotModule } from './plot'
import { QrModule } from './qr'
import { StudyAreaModule } from './studyarea'
import { PagesRoutingModule } from './pages-routing.module'
import { ThemeModule } from '../@theme/theme.module'

const PAGES_COMPONENTS = [
  PagesComponent,
]

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    PageNotFoundModule,
    ErrorMessageModule,
    CensusModule,
    PhotoPanoramaModule,
    PhotoUpDownModule,
    PhotopointUserModule,
    PlotModule,
    QrModule,
    StudyAreaModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
})
export class PagesModule {
}
