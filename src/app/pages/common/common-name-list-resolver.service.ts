import { ErrorHandler } from '@angular/core'
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router'

import { Observable, of } from 'rxjs'
import { map, take, catchError } from 'rxjs/operators'

import { CommonService } from './common.service'
import { fullErrorMessage } from '../path-fragments'

export type NameList = {id: string, name: string}[]

export abstract class CommonNameListResolver implements Resolve<NameList> {
  constructor(
    private service: CommonService<any>,
    private router: Router,
    private errorHandler: ErrorHandler,
  ) {}

  protected getNameList() {
    return this.service.getNameList() as Observable<NameList>
  }

  protected getNameListType() {
    return this.service.urlFragment
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<NameList> {
    return this.getNameList().pipe(
      catchError((err, caught) => {
        const msg = `Failed when GETting a '${this.getNameListType()}' name list, with ` +
          `error=${JSON.stringify(err, null, 2)}`
        this.errorHandler.handleError(new Error(msg))
        this.router.navigate([fullErrorMessage])
        return of([] as any)
      }),
      take(1),
      map(result => {
        if (result) {
          return result
        }
        const msg = `Data problem: successfully received response to ` +
          `GET a '${this.getNameListType()}' name list, but we don't think ` +
          `there is anything to display, result='${JSON.stringify(result, null, 2)}'.`
        this.errorHandler.handleError(new Error(msg))
        this.router.navigate([fullErrorMessage])
        return null
      }),
    )
  }
}
