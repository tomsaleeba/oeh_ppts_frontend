
import { _prepForUpdate } from './common.service'

describe('CommonService', () => {

  describe('._prepForUpdate()', function () {
    it('should replace stringified null when present', () => {
      const patch = {
        name: 'new name',
        affiliation: 'null',
      }
      const result = _prepForUpdate(patch)
      expect(result).toEqual({
        name: 'new name',
        affiliation: null,
      })
    })
  })

})
