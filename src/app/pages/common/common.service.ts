import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

import { environment } from '../../../environments/environment'
import { v1Mime } from '../path-fragments'

@Injectable()
export abstract class CommonService<T extends CommonModel> {

  private headers = new HttpHeaders({
    'Accept': v1Mime(this.mimeFragment ? this.mimeFragment : this.urlFragment),
    'x-api-key': environment.apiKey,
  })

  constructor(
    protected http: HttpClient,
    readonly urlFragment: string,
    readonly urlForSingleRecord: string,
    private mimeFragment?: string,
  ) { }

  getAll(params?): Observable<T[]> {
    const url = this.urlFragment
    const resp = this.doGet(url, params) as Observable<T[]>
    return resp.pipe(map(val => {
      // this is a bit dirty. It's hard to get values into the cell renderer
      // component, so we inject it into the data. Not pretty but it works.
      return val.map(curr => {
        (curr as any).urlForSingleRecord = this.urlForSingleRecord
        return curr
      })
    }))
  }

  getOne(id: string): Observable<T> {
    // TODO validate ID
    const url = `${this.urlFragment}/${id}`
    return this.doGet(url) as Observable<T>
  }

  getNameList() {
    const url = this.urlFragment
    return this.doGet(url, {
      select: 'name',
      populate: false,
      sort: 'name',
      limit: 99999,
    })
  }

  create(obj: CommonModel): Observable<T> {
    const url = this.urlFragment
    const cleanedObj = removeNullFields(obj)
    return this.doPost(url, cleanedObj) as Observable<T>
  }

  update(recordId: string | number, patch: CommonModel): Observable<T> {
    const url = `${this.urlFragment}/${recordId}`
    return this.doPatch(url, _prepForUpdate(patch)) as Observable<T>
  }

  protected doGet(url, params?) {
    // TODO handle failure
    return this.http.get(apiUrl(url), {
      headers: this.headers,
      params: params,
    })
  }

  private doPost(url, obj) {
    // TODO handle failure
    return this.http.post(apiUrl(url), obj, {
      headers: this.headers,
    })
  }

  private doPatch(url, obj) {
    // TODO handle failure
    return this.http.patch(apiUrl(url), obj, {
      headers: this.headers,
    })
  }
}

export class CommonModel {
  readonly id: string
  readonly createdAt: number
  readonly updatedAt: number
}

function removeNullFields (obj) {
  for (const curr of Object.keys(obj)) {
    if (obj[curr] === null) {
      delete obj[curr]
    }
  }
  return obj
}

export const nullButStringifiedBecauseTheSelectFormControlDoesThatForSomeReason = 'null'

export function _prepForUpdate (patch) {
  const result = Object.keys(patch).reduce((accum, curr) => {
    const value = nullButStringifiedBecauseTheSelectFormControlDoesThatForSomeReason === patch[curr] ? null : patch[curr]
    accum[curr] = value
    return accum
  }, {})
  return result
}

export function apiUrl (suffix: string) {
  return `${environment.apiUrl}/${suffix}`
}
