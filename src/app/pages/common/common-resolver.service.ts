import { Injectable, ErrorHandler } from '@angular/core'
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router'

import { Observable, of } from 'rxjs'
import { map, take, catchError } from 'rxjs/operators'

import { CommonService, CommonModel } from './common.service'
import { fullErrorMessage } from '../path-fragments'

@Injectable()
export abstract class CommonDataResolver<T extends CommonModel> implements Resolve<T> {
  constructor(
    private service: CommonService<T>,
    private router: Router,
    private errorHandler: ErrorHandler,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<T> {
    const id = route.queryParamMap.get('id')
    return this.service.getOne(id).pipe(
      catchError((err, caught) => {
        const msg = `Failed when GETting one '${this.service.urlFragment}' for id='${id}', error=${JSON.stringify(err, null, 2)}`
        this.errorHandler.handleError(new Error(msg))
        this.router.navigate([fullErrorMessage])
        return of({}) as Observable<T>
      }),
      take(1),
      map(result => {
        if (result) {
          return result
        }
        this.errorHandler.handleError(new Error(`Data problem: successfully received response to ` +
          `GET a single '${this.service.urlFragment}' record with id='${id}', but we don't think ` +
          `there is anything to display, result='${JSON.stringify(result, null, 2)}'.`))
        this.router.navigate([fullErrorMessage])
        return null
      }),
    )
  }
}
