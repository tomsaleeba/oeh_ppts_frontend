import { Component } from '@angular/core'

import { ICellRendererAngularComp } from 'ag-grid-angular'

@Component({
  templateUrl: './linkable-id.component.html',
})
export class LinkableIdComponent implements ICellRendererAngularComp {

  params: any

  agInit(params: any) {
    this.params = params
  }

  refresh(): boolean {
    return true
  }

  isData() {
    return typeof(this.params.data) !== 'undefined'
  }

  getId() {
    if (!this.isData()) {
      return null
    }
    return this.params.data.id
  }

  getTitle() {
    if (!this.isData()) {
      return null
    }
    const fieldForName = this.params.colDef.colId
    return this.params.data[fieldForName]
  }

  getTargetUrl() {
    if (!this.isData()) {
      return null
    }
    return this.params.data.urlForSingleRecord
  }
}
