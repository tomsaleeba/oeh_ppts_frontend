import { Injectable } from '@angular/core'
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router'

import { Observable } from 'rxjs'

import { VocabService } from './vocab.service'

@Injectable()
export class VocabResolver implements Resolve<String> {
  constructor(
    private service: VocabService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<String> {
    return this.service.getVocab()
  }
}

