import { Injectable } from '@angular/core'
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router'

import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

import { ConfigFromServerService } from './config-from-server.service'

@Injectable()
export class ThumbnailBaseUrlResolver implements Resolve<String> {
  constructor(
    private service: ConfigFromServerService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<String> {
    return this.service.getCache().pipe(
      map(result => {
        return result.thumbnailBaseUrl
      }),
    )
  }
}

