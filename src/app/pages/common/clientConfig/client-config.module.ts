import { NgModule } from '@angular/core'
import { ConfigFromServerService } from './config-from-server.service'
import { TusEndpointResolver } from './tus-endpoint-resolver.service'
import { ThumbnailBaseUrlResolver } from './thumbnail-base-url-resolver.service'
import { VocabResolver } from './vocab-resolver.service'
import { VocabService } from './vocab.service'

@NgModule({
  declarations: [
  ],
  providers: [
    ConfigFromServerService,
    TusEndpointResolver,
    ThumbnailBaseUrlResolver,
    VocabResolver,
    VocabService,
  ],
})
export class ClientConfigModule {}
