import { Injectable, ErrorHandler } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { map, take, catchError } from 'rxjs/operators'
import { Router } from '@angular/router'

import { environment } from '../../../../environments/environment'
import { fullErrorMessage } from '../../path-fragments'

@Injectable()
export class VocabService {
  private cache: any
  private headers = new HttpHeaders({
    'x-api-key': environment.apiKey,
  })

  constructor(
    private http: HttpClient,
    private router: Router,
    private errorHandler: ErrorHandler,
  ) {}

  getVocab (): Observable<any> {
    if (this.cache) {
      return of(this.cache)
    }
    return this.http.get(environment.apiUrl + '/vocab', { headers: this.headers }).pipe(
      catchError((err, caught) => {
        const msg = `Failed when GETting the vocabularies, error=${JSON.stringify(err, null, 2)}`
        this.errorHandler.handleError(new Error(msg))
        this.router.navigate([fullErrorMessage])
        return of(null) as Observable<any>
      }),
      take(1),
      map(result => {
        if (result) {
          this.cache = result
          return result
        }
        this.errorHandler.handleError(new Error(`Data problem: successfully received response to ` +
          `GET the vocabs, but there is no value.`))
        this.router.navigate([fullErrorMessage])
        // FIXME can we throw an error from here?
        return null
      }),
    )
  }
}

