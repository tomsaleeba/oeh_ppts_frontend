import { Injectable, ErrorHandler } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { map, take, catchError } from 'rxjs/operators'
import { Router } from '@angular/router'

import { environment } from '../../../../environments/environment'
import { fullErrorMessage } from '../../path-fragments'

@Injectable()
export class ConfigFromServerService {
  private configCache: any
  private headers = new HttpHeaders({
    'x-api-key': environment.apiKey,
  })

  constructor(
    private http: HttpClient,
    private router: Router,
    private errorHandler: ErrorHandler,
  ) {}

  getCache (): Observable<any> {
    if (this.configCache) {
      return of(this.configCache)
    }
    return this.http.get(environment.apiUrl + '/config', { headers: this.headers }).pipe(
      catchError((err, caught) => {
        const msg = `Failed when GETting the client config, error=${JSON.stringify(err, null, 2)}`
        this.errorHandler.handleError(new Error(msg))
        this.router.navigate([fullErrorMessage])
        return of(null) as Observable<any>
      }),
      take(1),
      map(result => {
        if (result) {
          this.configCache = result
          return result
        }
        this.errorHandler.handleError(new Error(`Data problem: successfully received response to ` +
          `GET the client config, but there is no value.`))
        this.router.navigate([fullErrorMessage])
        // FIXME can we throw an error from here?
        return null
      }),
    )
  }
}

