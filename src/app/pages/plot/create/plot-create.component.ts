import { Component, ErrorHandler } from '@angular/core'
import { Location } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router'
import { plainTextField, selectField, numberField } from '../../../formComponents'

import { PlotService } from '../plot.service'
import { fullPlotSingle } from '../../path-fragments'
import { CommonCreateComponent } from '../../../formComponents'

@Component({
  templateUrl: '../../../formComponents/commonCreate/common-create.component.html',
})
export class PlotCreateComponent extends CommonCreateComponent {

  orgNameList: [{}]

  constructor(
    readonly location: Location,
    readonly route: ActivatedRoute,
    readonly router: Router,
    readonly service: PlotService,
    readonly errorHandler: ErrorHandler,
  ) {
    super(location, route, router, service, fullPlotSingle, 'Plot',
      errorHandler, PlotCreateComponent.getFields(route))
  }

  static getFields(route) {
    const result = []
    route.data.subscribe(data => {
      const isEditMode = data.isEditMode
      const latMin = -43.634
      const latMax = -10.669
      const longMin = 113.338
      const longMax = 153.57
      result.push(
        plainTextField('name', 'Plot name', isEditMode, {isRequired: true, maxLength: 200}),
        selectField('studyArea', 'Study area', route.data, 'studyAreaList'),
        selectField('contact', 'Contact', route.data, 'userList'),
        numberField('lat', `Latitude (${latMin} < value < ${latMax})`, false, {
          isRequired: true,
          min: latMin,
          max: latMax,
        }),
        numberField('long', `Longitude (${longMin} < value < ${longMax})`, true, {
          isRequired: true,
          min: longMin,
          max: longMax,
        }),
      )
      if (!data.isEditMode) {
        result.push(
          selectField('ibraCode', 'IBRA code', route.data, 'ibraList', {isRequired: true}),
        )
      }
    })
    return result
  }

  prepModelForEdit() {
    if (this.model.studyArea) {
      this.model.studyArea = this.model.studyArea.id
    }
    if (this.model.contact) {
      this.model.contact = this.model.contact.id
    }
  }

}
