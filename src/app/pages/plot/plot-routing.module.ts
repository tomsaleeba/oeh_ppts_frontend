import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { IbraNameListResolver } from './resolvers/ibra-name-list-resolver.service'
import { PhotopointUserNameListResolver } from '../photopoint-user/resolvers/photopoint-user-name-list-resolver.service'
import { StudyAreaNameListResolver } from '../studyarea/resolvers/studyarea-name-list-resolver.service'

import { PlotComponent } from './plot.component'
import { PlotAllComponent } from './all/plot-all.component'
import { PlotSingleComponent } from './single/plot-single.component'
import { PlotDataResolver } from './single/plot-single-resolver.service'
import { PlotCreateComponent } from './create/plot-create.component'
import { all, single, create, edit } from '../path-fragments'

const routes: Routes = [{
  path: '',
  component: PlotComponent,
  children: [{
    path: all,
    component: PlotAllComponent,
  }, {
    path: single,
    component: PlotSingleComponent,
    resolve: {
      plot: PlotDataResolver,
    },
  }, {
    path: create,
    component: PlotCreateComponent,
    resolve: {
      ibraList: IbraNameListResolver,
      studyAreaList: StudyAreaNameListResolver,
      userList: PhotopointUserNameListResolver,
    },
  }, {
    path: edit,
    component: PlotCreateComponent,
    data: {
      isEditMode: true,
    },
    resolve: {
      existing: PlotDataResolver,
      studyAreaList: StudyAreaNameListResolver,
      userList: PhotopointUserNameListResolver,
    },
  }],
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlotRoutingModule {
}
