import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { CommonDataResolver } from '../../common'
import { PlotService, Plot } from '../plot.service'

@Injectable()
export class PlotDataResolver extends CommonDataResolver<Plot> {
  constructor(service: PlotService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
