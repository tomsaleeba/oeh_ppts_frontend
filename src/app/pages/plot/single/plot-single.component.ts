import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { Plot } from '../plot.service'
import { OehMapMarker } from '../../../oehComponents/oehMap/oeh-map.component'

@Component({
  templateUrl: './plot-single.component.html',
  styles: [`
    table tbody tr th {
      width: 15em
    }
  `],
})
export class PlotSingleComponent implements OnInit {

  plotData: Plot
  markers: OehMapMarker[] = []
  secondaryMarkers: OehMapMarker[] = []

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: {plot: Plot}) => {
      this.plotData = data.plot
      this.markers.push({
        lat: this.plotData.lat,
        long: this.plotData.long,
        label: `Plot: ${this.plotData.name}`,
      })
      const isCensusPresent = this.plotData.census && this.plotData.census.length
      if (isCensusPresent) {
        this.secondaryMarkers = this.plotData.census.reduce((accum, curr) => {
          accum.push({
            lat: curr.lat,
            long: curr.long,
            label: `Census: ${curr.name}`,
          })
          return accum
        }, [])
      }
    })
  }

  isCoordsRecorded() {
    return this.plotData.lat && this.plotData.long
  }
}
