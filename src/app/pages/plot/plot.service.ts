import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators'

import { environment } from '../../../environments/environment'
import { CommonService, CommonModel, apiUrl } from '../common/common.service'
import { v1Mime, fullPlotSingle } from '../path-fragments'

@Injectable()
export class PlotService extends CommonService<Plot> {

  constructor(
    http: HttpClient,
  ) {
    super(http, 'plot', fullPlotSingle)
  }

  getAll() {
    const superResult = super.getAll()
    return superResult.pipe(
      map((plots: any) => {
        return plots.reduce((accum, curr) => {
          // Note: all plots *should* have a plot code
          if (!curr.plotCode || curr.plotCode.length === 0) {
            curr.plotCode = null
            accum.push(curr)
            return accum
          }
          curr.plotCode = curr.plotCode[0].full
          accum.push(curr)
          return accum
        }, [])
      }),
    )
  }

  getOne(id: string) {
    const superResult = super.getOne(id)
    return superResult.pipe(
      map((plot: any) => {
        if (!plot.plotCode || plot.plotCode.length === 0) {
          plot.plotCode = null
          return plot
        }
        plot.plotCode = plot.plotCode[0].full
        return plot
      }),
    )
  }

  getIbraList() {
    return this.http.get(apiUrl(`plotcode/ibra-list`), {
      headers: {
        'Accept': v1Mime('plotcode'),
        'x-api-key': environment.apiKey,
      },
    }).pipe(
      map((ibraList: string[]) => {
        const result = ibraList.reduce((accum, curr) => {
          accum.push({id: curr, name: curr})
          return accum
        }, [])
        return result
      }),
    )
  }
}

export class Plot extends CommonModel {
  constructor (
    readonly name: string,
    readonly lat: number,
    readonly long: number,
    readonly altitudeMetres: number,
    readonly locationAccuracy: number,
    readonly studyArea: any,
    readonly contact: any,
    readonly qr: any[],
    readonly census: any[],
    readonly plotCode: any[],
  ) {
    super()
  }
}
