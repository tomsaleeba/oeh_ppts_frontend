import { TestBed, inject } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { PlotService, Plot } from './plot.service'
import { environment } from '../../../environments/environment'

describe('PlotService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PlotService],
    })
  })

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify()
  })))

  it('should be created', inject([PlotService], (service: PlotService) => {
    expect(service).toBeTruthy()
  }))

  it('should return all plots', inject([PlotService, HttpTestingController],
      (service: PlotService, httpMock: HttpTestingController) => {
    service.getAll().subscribe((result: Plot[]) => {
      expect(result.length).toBe(2)
      expect(result[0].plotCode).toBeNull()
      expect(result[1].plotCode).toBe('NSWPAUA000003')
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/plot`)
    expect(req.request.method).toBe('GET')
    req.flush([{
      id: 1,
      plotCode: [],
    }, {
      id: 2,
      plotCode: [{full: 'NSWPAUA000003'}],
    }])
  }))

  it('should return one plot', inject([PlotService, HttpTestingController],
      (service: PlotService, httpMock: HttpTestingController) => {
    service.getOne('111').subscribe((result: Plot) => {
      expect(result.plotCode).toBe('NSWPAUA000003')
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/plot/111`)
    expect(req.request.method).toBe('GET')
    req.flush({
      id: '111',
      plotCode: [{full: 'NSWPAUA000003'}],
    })
  }))
})
