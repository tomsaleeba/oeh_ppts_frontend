import { NgModule } from '@angular/core'

import { OehComponentsModule } from '../../oehComponents'
import { FormComponentsModule } from '../../formComponents'
import { ThemeModule } from '../../@theme/theme.module'
import { PlotRoutingModule } from './plot-routing.module'
import { PlotComponent } from './plot.component'
import { PlotAllComponent } from './all/plot-all.component'
import { PlotSingleComponent } from './single/plot-single.component'
import { PlotService } from './plot.service'
import { PlotDataResolver } from './single/plot-single-resolver.service'
import { PlotNameListResolver } from './resolvers/plot-name-list-resolver.service'
import { IbraNameListResolver } from './resolvers/ibra-name-list-resolver.service';
import { PlotCreateComponent } from './create/plot-create.component'

@NgModule({
  imports: [
    ThemeModule,
    PlotRoutingModule,
    OehComponentsModule,
    FormComponentsModule,
  ],
  declarations: [
    PlotComponent,
    PlotAllComponent,
    PlotSingleComponent,
    PlotCreateComponent,
  ],
  providers: [
    PlotService,
    PlotDataResolver,
    PlotNameListResolver,
    IbraNameListResolver,
  ],
})
export class PlotModule { }
