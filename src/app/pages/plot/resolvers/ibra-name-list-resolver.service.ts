import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'
import { Observable } from 'rxjs'

import { PlotService } from '../plot.service'
import { CommonNameListResolver, NameList } from '../../common/common-name-list-resolver.service'

@Injectable()
export class IbraNameListResolver extends CommonNameListResolver {
  constructor(public plotService: PlotService, router: Router, errorHandler: ErrorHandler) {
    super(plotService, router, errorHandler)
  }

  protected getNameList() {
    return this.plotService.getIbraList() as Observable<NameList>
  }

  protected getNameListType() {
    return 'IBRA'
  }
}
