import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { PlotService } from '../plot.service'
import { CommonNameListResolver } from '../../common/common-name-list-resolver.service'

@Injectable()
export class PlotNameListResolver extends CommonNameListResolver {
  constructor(service: PlotService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
