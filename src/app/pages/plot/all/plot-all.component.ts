import { Component } from '@angular/core'

import { PlotService } from '../plot.service'

@Component({
  templateUrl: './plot-all.component.html',
})
export class PlotAllComponent {

  constructor(
    public plotService: PlotService,
  ) {}

  settings = [
    {
      field: 'name',
      title: 'Name',
      type: 'id',
    }, {
      field: 'census',
      title: 'Census count',
      type: 'number',
      isCount: true,
    }, {
      field: 'plotCode',
      title: 'Plot Code',
      type: 'text',
    }, {
      field: 'lat',
      title: 'Latitude',
      type: 'number',
    }, {
      field: 'long',
      title: 'Longitude',
      type: 'number',
    }, {
      field: 'altitudeMetres',
      title: 'Altitude (metres)',
      type: 'number',
    }, {
      field: 'contact',
      title: 'Contact',
      type: 'link',
    }, {
      field: 'studyArea',
      title: 'Study area',
      type: 'link',
    },
  ]
}
