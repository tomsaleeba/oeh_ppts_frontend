import { Component } from '@angular/core'

import { MENU_ITEMS } from './pages-menu'

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-layout>
  `,
  styles: [`
    nb-menu {
      padding-bottom: 5em;
    }
  `],
})
export class PagesComponent {

  menu = MENU_ITEMS
}
