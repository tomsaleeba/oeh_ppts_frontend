import { NgModule } from '@angular/core'

import { ThemeModule } from '../../@theme/theme.module'
import { PageNotFoundComponent } from './page-not-found.component'

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    PageNotFoundComponent,
  ],
})
export class PageNotFoundModule { }
