import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { PhotopointUserService } from '../photopoint-user.service'

@Component({
  templateUrl: './photopoint-user-all.component.html',
})
export class PhotopointUserAllComponent {

  settings: any[]

  constructor(
    public photopointUserService: PhotopointUserService,
    private route: ActivatedRoute,
  ) {
    this.route.data.subscribe(data => {
      this.settings = [
        {
          field: 'name',
          title: 'Name',
          type: 'id',
        }, {
          field: 'email',
          title: 'Email',
          type: 'text',
        }, {
          field: 'affiliation',
          title: 'Affiliation',
          type: 'link',
        }, {
          field: 'type',
          title: 'Type',
          type: 'vocabText',
          vocab: data.vocab.userTypes,
        }, {
          field: 'coordinatorOf',
          title: '# Study areas coordinated',
          type: 'number',
          isCount: true,
        }, {
          field: 'contactFor',
          title: 'Contact for # plots',
          type: 'number',
          isCount: true,
        }, {
          field: 'collectorFor',
          title: 'Collector for # censuses',
          type: 'number',
          isCount: true,
        },
      ]
    })
  }

}
