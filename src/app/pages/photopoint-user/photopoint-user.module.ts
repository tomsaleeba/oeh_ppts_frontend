import { NgModule } from '@angular/core'

import { OehComponentsModule } from '../../oehComponents'
import { FormComponentsModule } from '../../formComponents'
import { ThemeModule } from '../../@theme/theme.module'
import { PhotopointUserRoutingModule } from './photopoint-user-routing.module'
import { PhotopointUserComponent } from './photopoint-user.component'
import { PhotopointUserAllComponent } from './all/photopoint-user-all.component'
import { PhotopointUserSingleComponent } from './single/photopoint-user-single.component'
import { PhotopointUserService } from './photopoint-user.service'
import { PhotopointUserDataResolver } from './single/photopoint-user-single-resolver.service'
import { PhotopointUserCreateComponent } from './create/photopoint-user-create.component'
import { PhotopointUserNameListResolver } from './resolvers/photopoint-user-name-list-resolver.service'
import { UserTypeListResolver } from './resolvers/user-type-list-resolver.service'
import { OrganisationModule } from '../organisation'

@NgModule({
  imports: [
    ThemeModule,
    PhotopointUserRoutingModule,
    OehComponentsModule,
    FormComponentsModule,
    OrganisationModule,
  ],
  declarations: [
    PhotopointUserComponent,
    PhotopointUserAllComponent,
    PhotopointUserSingleComponent,
    PhotopointUserCreateComponent,
  ],
  providers: [
    PhotopointUserService,
    PhotopointUserDataResolver,
    PhotopointUserNameListResolver,
    UserTypeListResolver,
  ],
})
export class PhotopointUserModule { }
