import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { OrgNameListResolver } from '../organisation/resolvers/org-name-list-resolver.service'
import { PlotNameListResolver } from '../plot/resolvers/plot-name-list-resolver.service'
import { StudyAreaNameListResolver } from '../studyarea/resolvers/studyarea-name-list-resolver.service'
import { UserTypeListResolver } from './resolvers/user-type-list-resolver.service'

import { PhotopointUserComponent } from './photopoint-user.component'
import { PhotopointUserAllComponent } from './all/photopoint-user-all.component'
import { PhotopointUserSingleComponent } from './single/photopoint-user-single.component'
import { PhotopointUserDataResolver } from './single/photopoint-user-single-resolver.service'
import { PhotopointUserCreateComponent } from './create/photopoint-user-create.component'
import { all, single, create, edit } from '../path-fragments'
import { VocabResolver } from '../common/clientConfig/vocab-resolver.service'

const routes: Routes = [{
  path: '',
  component: PhotopointUserComponent,
  children: [{
    path: all,
    component: PhotopointUserAllComponent,
    resolve: {
      vocab: VocabResolver,
    },
  }, {
    path: single,
    component: PhotopointUserSingleComponent,
    resolve: {
      vocab: VocabResolver,
      photopointUser: PhotopointUserDataResolver,
    },
  }, {
    path: create,
    component: PhotopointUserCreateComponent,
    resolve: {
      orgList: OrgNameListResolver,
      plotList: PlotNameListResolver,
      studyAreaList: StudyAreaNameListResolver,
      userTypeList: UserTypeListResolver,
    },
  }, {
    path: edit,
    component: PhotopointUserCreateComponent,
    data: {
      isEditMode: true,
    },
    resolve: {
      existing: PhotopointUserDataResolver,
      orgList: OrgNameListResolver,
      plotList: PlotNameListResolver,
      studyAreaList: StudyAreaNameListResolver,
      userTypeList: UserTypeListResolver,
    },
  }],
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhotopointUserRoutingModule {
}
