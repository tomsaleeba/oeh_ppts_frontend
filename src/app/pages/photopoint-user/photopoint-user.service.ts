import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { CommonService, CommonModel } from '../common/common.service'
import { fullPhotopointUserSingle } from '../path-fragments'

@Injectable()
export class PhotopointUserService extends CommonService<PhotopointUser> {

  constructor(
    http: HttpClient,
  ) {
    super(http, 'user', fullPhotopointUserSingle)
  }
}

export class PhotopointUser extends CommonModel {
  constructor (
    readonly name: string,
    // TODO add more fields
  ) {
    super()
  }
}
