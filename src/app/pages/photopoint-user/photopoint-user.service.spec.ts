import { TestBed, inject } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { PhotopointUserService } from './photopoint-user.service'
import { environment } from '../../../environments/environment'

describe('PhotopointUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PhotopointUserService],
    })
  })

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify()
  })))

  it('should be created', inject([PhotopointUserService], (service: PhotopointUserService) => {
    expect(service).toBeTruthy()
  }))

  it('should return all photopoint users', inject([PhotopointUserService, HttpTestingController],
      (service: PhotopointUserService, httpMock: HttpTestingController) => {
    service.getAll().subscribe((result) => {
      expect(result.length).toBe(2)
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/user`)
    expect(req.request.method).toBe('GET')
    req.flush([{id: 1}, {id: 2}])
  }))
})
