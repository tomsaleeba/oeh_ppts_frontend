import { Component, ErrorHandler } from '@angular/core'
import { Location } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router'
import { emailField, plainTextField, selectField, multiSelectField } from '../../../formComponents'

import { PhotopointUserService } from '../photopoint-user.service'
import { fullPhotopointUserSingle } from '../../path-fragments'
import { CommonCreateComponent } from '../../../formComponents'

@Component({
  templateUrl: '../../../formComponents/commonCreate/common-create.component.html',
})
export class PhotopointUserCreateComponent extends CommonCreateComponent {

  orgNameList: [{}]

  constructor(
    readonly location: Location,
    readonly route: ActivatedRoute,
    readonly router: Router,
    readonly service: PhotopointUserService,
    readonly errorHandler: ErrorHandler,
  ) {
    super(location, route, router, service, fullPhotopointUserSingle, 'Photopoint user',
      errorHandler, PhotopointUserCreateComponent.getFields(route))
  }

  static getFields(route) {
    const result = []
    route.data.subscribe(data => {
      const isEditMode = data.isEditMode
      result.push(
        plainTextField('name', 'User\'s full name', isEditMode, {isRequired: true, maxLength: 100}),
        emailField('email', 'Email address', isEditMode),
        selectField('affiliation', 'Affiliation', route.data, 'orgList'),
        selectField('type', 'User type', route.data, 'userTypeList', {isRequired: true}),
        multiSelectField('coordinatorOf', 'Coordinator of', route.data, 'studyAreaList'),
        multiSelectField('contactFor', 'Contact for', route.data, 'plotList'),
      )
    })
    return result
  }

  prepModelForEdit() {
    if (this.model.affiliation) {
      this.model.affiliation = this.model.affiliation.id
    }
  }
}
