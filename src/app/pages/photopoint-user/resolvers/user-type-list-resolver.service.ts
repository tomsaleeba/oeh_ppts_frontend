import { Injectable } from '@angular/core'
import { Resolve } from '@angular/router'

import { Observable } from 'rxjs'
import { pluck, map } from 'rxjs/operators'

import { NameList } from '../../common/common-name-list-resolver.service'
import { VocabService } from '../../common/clientConfig/vocab.service'

@Injectable()
export class UserTypeListResolver implements Resolve<NameList> {

  constructor(
    private vocabService: VocabService,
  ) {}

  resolve(): Observable<any> {
    return this.vocabService.getVocab().pipe(
      pluck('userTypes'),
      map((e: [{name: string, abbreviation: string}]) => {
        return e.map(currVocabRec => {
          return { id: currVocabRec.abbreviation, name: currVocabRec.name }
        })
      }),
    )
  }
}
