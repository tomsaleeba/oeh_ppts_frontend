import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { PhotopointUserService } from '../photopoint-user.service'
import { CommonNameListResolver } from '../../common/common-name-list-resolver.service'

@Injectable()
export class PhotopointUserNameListResolver extends CommonNameListResolver {
  constructor(service: PhotopointUserService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
