import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { PhotopointUserService, PhotopointUser } from '../photopoint-user.service'
import { CommonDataResolver } from '../../common'

@Injectable()
export class PhotopointUserDataResolver extends CommonDataResolver<PhotopointUser> {
  constructor(service: PhotopointUserService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
