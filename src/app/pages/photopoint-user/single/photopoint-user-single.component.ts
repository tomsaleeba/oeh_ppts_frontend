import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'


@Component({
  templateUrl: './photopoint-user-single.component.html',
  styles: [`
    table tbody tr th {
      width: 15em
    }
  `],
})
export class PhotopointUserSingleComponent implements OnInit {

  userData: any

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.userData = data.photopointUser
      const matchingVocab = data.vocab.userTypes.find(e => this.userData.type === e.abbreviation)
      if (matchingVocab) {
        this.userData.type = matchingVocab.name
      }
    })
  }
}
