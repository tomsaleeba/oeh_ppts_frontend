import { NbMenuItem } from '@nebular/theme'

import * as pf from './path-fragments'

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  }, {
    title: 'Censuses',
    icon: 'nb-compose',
    link: pf.fullCensus,
    children: [{
      title: 'All Censuses',
      link: pf.fullCensusAll,
    }],
  }, {
    title: 'Organisations',
    icon: 'nb-roller-shades',
    link: pf.fullOrganisation,
    children: [{
      title: 'All Organisations',
      link: pf.fullOrganisationAll,
    }, {
      title: 'Create Organisation',
      link: pf.fullOrganisationCreate,
    }],
  }, {
    title: 'Panorama Photos',
    icon: 'nb-sunny-circled',
    link: pf.fullPhotoPanorama,
    children: [{
      title: 'All Panorama Photos',
      link: pf.fullPhotoPanoramaAll,
    }, {
      title: 'Bulk upload',
      link: pf.fullPhotoPanoramaBulk,
    }],
  }, {
    title: 'Up/Down Photos',
    icon: 'nb-arrow-retweet',
    link: pf.fullPhotoUpDown,
    children: [{
      title: 'All Photos (tabular)',
      link: pf.fullPhotoUpDownAll,
    }],
  }, {
    title: 'Photopoint users',
    link: pf.fullPhotopointUser,
    icon: 'nb-person',
    children: [{
      title: 'All Photopoint Users',
      link: pf.fullPhotopointUserAll,
    }, {
      title: 'Create photopoint user',
      link: pf.fullPhotopointUserCreate,
    }],
  }, {
    title: 'Plots',
    icon: 'nb-grid-a',
    link: pf.fullPlot,
    children: [{
      title: 'All Plots',
      link: pf.fullPlotAll,
    }, {
      title: 'Create plot',
      link: pf.fullPlotCreate,
    }],
  }, {
    title: 'QR Codes',
    icon: 'nb-grid-b-outline',
    link: pf.fullQr,
    children: [{
      title: 'All QR Codes',
      link: pf.fullQrAll,
    }],
  }, {
    title: 'Study Areas',
    link: pf.fullStudyArea,
    icon: 'nb-location',
    children: [{
      title: 'All Study Areas',
      link: pf.fullStudyAreaAll,
    }, {
      title: 'Create study area',
      link: pf.fullStudyAreaCreate,
    }],
  },
]
