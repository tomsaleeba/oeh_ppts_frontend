import { TestBed, inject } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { OrganisationService } from './organisation.service'
import { environment } from '../../../environments/environment'

describe('OrganisationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [OrganisationService],
    })
  })

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify()
  })))

  it('should be created', inject([OrganisationService], (service: OrganisationService) => {
    expect(service).toBeTruthy()
  }))

  it('should return all organisations', inject([OrganisationService, HttpTestingController],
      (service: OrganisationService, httpMock: HttpTestingController) => {
    service.getAll().subscribe((result) => {
      expect(result.length).toBe(2)
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/organisation`)
    expect(req.request.method).toBe('GET')
    req.flush([{id: 1}, {id: 2}])
  }))
})
