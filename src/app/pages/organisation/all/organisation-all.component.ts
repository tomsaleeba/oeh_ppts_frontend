import { Component } from '@angular/core'

import { OrganisationService } from '../organisation.service'

@Component({
  templateUrl: './organisation-all.component.html',
})
export class OrganisationAllComponent {

  constructor(
    public organisationService: OrganisationService,
  ) {}

  settings = [
    {
      field: 'name',
      title: 'Name',
      type: 'id',
    }, {
      field: 'contactEmail',
      title: 'Contact Email',
      type: 'text',
    }, {
      field: 'address',
      title: 'Address',
      type: 'text',
    }, {
      field: 'website',
      title: 'Website',
      type: 'text',
    }, {
      field: 'members',
      title: 'Member count',
      type: 'number',
      isCount: true,
    },
  ]
}
