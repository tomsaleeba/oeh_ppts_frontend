import { Component, ErrorHandler } from '@angular/core'
import { Location } from '@angular/common'
import { emailField, urlField, plainTextField, multiSelectField } from '../../../formComponents'

import { OrganisationService } from '../organisation.service'
import { fullOrganisationSingle } from '../../path-fragments'
import { CommonCreateComponent } from '../../../formComponents'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  templateUrl: '../../../formComponents/commonCreate/common-create.component.html',
})
export class OrganisationCreateComponent extends CommonCreateComponent {

  constructor(
    readonly location: Location,
    readonly route: ActivatedRoute,
    readonly router: Router,
    readonly service: OrganisationService,
    readonly errorHandler: ErrorHandler,
  ) {
    super(location, route, router, service, fullOrganisationSingle, 'Organisation',
      errorHandler, OrganisationCreateComponent.getFields(route))
  }

  static getFields(route) {
    const result = []
    route.data.subscribe(data => {
      const isEditMode = data.isEditMode
      result.push(
        plainTextField('name', 'Organisation name', isEditMode, {isRequired: true, maxLength: 200}),
        emailField('contactEmail', 'Contact email address', isEditMode),
        plainTextField('address', 'Postal address', isEditMode),
        urlField('website', 'Website address', isEditMode),
        multiSelectField('members', 'Members', route.data, 'userList'),
      )
    })
    return result
  }

}
