import { NgModule } from '@angular/core'

import { OehComponentsModule } from '../../oehComponents'
import { FormComponentsModule } from '../../formComponents'
import { ThemeModule } from '../../@theme/theme.module'
import { OrganisationRoutingModule } from './organisation-routing.module'
import { OrganisationComponent } from './organisation.component'
import { OrganisationAllComponent } from './all/organisation-all.component'
import { OrganisationSingleComponent } from './single/organisation-single.component'
import { OrganisationCreateComponent } from './create/organisation-create.component'
import { OrganisationService } from './organisation.service'
import { OrganisationDataResolver } from './single/organisation-single-resolver.service'
import { OrgNameListResolver } from './resolvers/org-name-list-resolver.service'

@NgModule({
  imports: [
    ThemeModule,
    OrganisationRoutingModule,
    OehComponentsModule,
    FormComponentsModule,
  ],
  declarations: [
    OrganisationComponent,
    OrganisationAllComponent,
    OrganisationSingleComponent,
    OrganisationCreateComponent,
  ],
  providers: [
    OrganisationService,
    OrganisationDataResolver,
    OrgNameListResolver,
  ],
})
export class OrganisationModule { }
