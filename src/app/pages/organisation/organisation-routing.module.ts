import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { PhotopointUserNameListResolver } from '../photopoint-user/resolvers/photopoint-user-name-list-resolver.service'

import { OrganisationComponent } from './organisation.component'
import { OrganisationAllComponent } from './all/organisation-all.component'
import { OrganisationSingleComponent } from './single/organisation-single.component'
import { OrganisationDataResolver } from './single/organisation-single-resolver.service'
import { OrganisationCreateComponent } from './create/organisation-create.component'
import { all, single, create, edit } from '../path-fragments'

const routes: Routes = [{
  path: '',
  component: OrganisationComponent,
  children: [
    {
      path: all,
      component: OrganisationAllComponent,
    }, {
      path: single,
      component: OrganisationSingleComponent,
      resolve: {
        organisation: OrganisationDataResolver,
      },
    }, {
      path: create,
      component: OrganisationCreateComponent,
      resolve: {
        userList: PhotopointUserNameListResolver,
      },
    }, {
      path: edit,
      component: OrganisationCreateComponent,
      data: {
        isEditMode: true,
      },
      resolve: {
        existing: OrganisationDataResolver,
        userList: PhotopointUserNameListResolver,
      },
    },
  ],
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrganisationRoutingModule {
}
