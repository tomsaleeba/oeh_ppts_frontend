import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { CommonService, CommonModel } from '../common/common.service'
import { fullOrganisationSingle } from '../path-fragments'

@Injectable()
export class OrganisationService extends CommonService<Organisation> {

  constructor(
    http: HttpClient,
  ) {
    super(http, 'organisation', fullOrganisationSingle)
  }
}

export class Organisation extends CommonModel {
  constructor (
    readonly name: string,
    readonly contactEmail: string,
    readonly address: string,
    readonly website: string,
  ) {
    super()
  }
}
