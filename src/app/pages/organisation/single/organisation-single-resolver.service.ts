import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { OrganisationService, Organisation } from '../organisation.service'
import { CommonDataResolver } from '../../common'

@Injectable()
export class OrganisationDataResolver extends CommonDataResolver<Organisation> {
  constructor(service: OrganisationService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
