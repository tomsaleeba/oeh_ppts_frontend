import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { Organisation } from '../organisation.service'

@Component({
  templateUrl: './organisation-single.component.html',
  styles: [`
    table tbody tr th {
      width: 15em
    }
  `],
})
export class OrganisationSingleComponent implements OnInit {

  organisationData: any

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: {organisation: Organisation}) => {
      this.organisationData = data.organisation
    })
  }

  getWebsite() {
    const website = this.organisationData.website.toLowerCase()
    if (website.startsWith('http')) {
      return website
    }
    return `http://${website}`
  }
}
