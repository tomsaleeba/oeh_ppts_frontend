import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { OrganisationService } from '../organisation.service'
import { CommonNameListResolver } from '../../common/common-name-list-resolver.service'

@Injectable()
export class OrgNameListResolver extends CommonNameListResolver {
  constructor(service: OrganisationService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
