import { Component } from '@angular/core'

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styles: [`
    #model-overview {
      max-width: 50em;
    }
  `],
})
export class DashboardComponent {
}
