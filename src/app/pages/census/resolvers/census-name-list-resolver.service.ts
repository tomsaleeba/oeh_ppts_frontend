import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { CensusService } from '../census.service'
import { CommonNameListResolver } from '../../common/common-name-list-resolver.service'

@Injectable()
export class CensusNameListResolver extends CommonNameListResolver {
  constructor(service: CensusService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
