import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { CensusComponent } from './census.component'
import { CensusAllComponent } from './all/census-all.component'
import { CensusSingleComponent } from './single/census-single.component'
import { CensusDataResolver } from './single/census-single-resolver.service'
import { all, single } from '../path-fragments'
import { VocabResolver } from '../common/clientConfig/vocab-resolver.service'

const routes: Routes = [{
  path: '',
  component: CensusComponent,
  children: [{
    path: all,
    component: CensusAllComponent,
    resolve: {
      vocab: VocabResolver,
    },
  }, {
    path: single,
    component: CensusSingleComponent,
    resolve: {
      census: CensusDataResolver,
      vocab: VocabResolver,
    },
  }],
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CensusRoutingModule {
}
