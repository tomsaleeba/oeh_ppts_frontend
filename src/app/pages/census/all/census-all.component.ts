import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { CensusService } from '../census.service'

@Component({
  templateUrl: './census-all.component.html',
})
export class CensusAllComponent {

  settings: any[]

  constructor(
    public censusService: CensusService,
    private route: ActivatedRoute,
  ) {
    this.route.data.subscribe(data => {
      this.settings = [
        {
          field: 'name',
          title: 'Name',
          type: 'id',
        }, {
          field: 'censusStartDate',
          title: 'Census Date',
          type: 'date',
        }, {
          field: 'censusEndDate',
          title: 'Census Complete?',
          type: 'boolean',
        }, {
          field: 'censusType',
          title: 'Type',
          type: 'vocabText',
          vocab: data.vocab.censusTypes,
        }, {
          field: 'plot',
          title: 'Plot',
          type: 'link',
        }, {
          field: 'collector',
          title: 'Collector',
          type: 'link',
        }, {
          field: 'panoramaPhotos',
          title: 'Panorama Count',
          type: 'number',
          isCount: true,
        }, {
          field: 'upDownPhotos',
          title: 'Up/Down Count',
          type: 'number',
          isCount: true,
        },
      ]
    })
  }
}
