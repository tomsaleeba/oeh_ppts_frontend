import { TestBed, inject } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { CensusService } from './census.service'
import { environment } from '../../../environments/environment'

describe('CensusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CensusService],
    })
  })

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify()
  })))

  it('should be created', inject([CensusService], (service: CensusService) => {
    expect(service).toBeTruthy()
  }))

  it('should return all censuses', inject([CensusService, HttpTestingController],
      (service: CensusService, httpMock: HttpTestingController) => {
    service.getAll().subscribe((result) => {
      expect(result.length).toBe(2)
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/census`)
    expect(req.request.method).toBe('GET')
    req.flush([{id: 1}, {id: 2}])
  }))
})
