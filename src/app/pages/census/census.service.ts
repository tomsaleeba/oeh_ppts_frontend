import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { CommonService, CommonModel } from '../common/common.service'
import { fullCensusSingle } from '../path-fragments'

@Injectable()
export class CensusService extends CommonService<Census> {

  constructor(
    http: HttpClient,
  ) {
    super(http, 'census', fullCensusSingle)
  }
}

export class Census extends CommonModel {
  constructor (
    public name: string,
    public censusType: string,
    public censusStartDate: number,
    public censusEndDate: number,
    public deviceInfo: any,
    public lat: number,
    public long: number,
    public plot: any[],
    public measurements: any,
    public panoramaPhotos: any[],
    public upDownPhotos: any[],
  ) {
    super()
  }
}
