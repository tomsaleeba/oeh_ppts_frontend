import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { DatePipe } from '@angular/common'

import { Census } from '../census.service'
import { OehMapMarker } from '../../../oehComponents/oehMap/oeh-map.component'
import { fullPhotoUpDownGallery } from '../../path-fragments'

@Component({
  templateUrl: './census-single.component.html',
  styles: [`
    table tbody tr th {
      width: 15em
    }
  `],
})
export class CensusSingleComponent implements OnInit {

  censusData: Census
  censusId: string
  censusTypeLabel = { label: null }
  vegCommunityLabel = { label: null }
  markers: OehMapMarker[] = []
  secondaryMarkers: OehMapMarker[] = []
  isUpDownPhotos = true
  upDownPhotoCount = 0

  constructor(
    private route: ActivatedRoute,
    private datePipe: DatePipe,
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.censusId = params.get('id')
    })
    this.route.data.subscribe(data => {
      this.censusData = data.census
      this.upDownPhotoCount = this.censusData.upDownPhotos.length
      this.isUpDownPhotos = this.upDownPhotoCount > 0
      if (this.censusData.measurements.length > 0) {
        this.vegCommunityLabel.label = this.resolveVocabValue(
          data.vocab.communityVegFormation,
          this.censusData.measurements[0].communityVegFormation)
      }
      this.censusData.deviceInfo = this.censusData.deviceInfo.length > 0
        ? this.censusData.deviceInfo[0]
        : null
      this.censusTypeLabel.label = this.resolveVocabValue(data.vocab.censusTypes, this.censusData.censusType)
      if (this.isCoordsRecorded()) {
        this.markers.push({
          lat: this.censusData.lat,
          long: this.censusData.long,
          label: `Census: ${this.censusData.name}`,
        })
      }
      this.secondaryMarkers = this.censusData.upDownPhotos.reduce((accum, curr) => {
        accum.push({
          lat: curr.lat,
          long: curr.long,
          label: `Photo: ${curr.name}; transect=${curr.transectId}; seq=${curr.sequence}`,
        })
        return accum
      }, [])
    })
  }

  private resolveVocabValue (vocab, rawValue) {
    const matchingVocab = vocab.find(e => rawValue === e.abbreviation)
    if (!matchingVocab) {
      return rawValue
    }
    return matchingVocab.name
  }

  buildUpDownGalleryLink () {
    return `${fullPhotoUpDownGallery}/${this.censusId}`
  }

  getCensusEndDate() {
    const value = this.censusData.censusEndDate
    if (!value || value === 0) {
      return 'no'
    }
    const completedDate = this.datePipe.transform(value, 'dd-MM-yyyy')
    return `yes (${completedDate})`
  }

  isCoordsRecorded() {
    return this.censusData.lat && this.censusData.long
  }

  isBam() {
    return this.censusData.censusType.toUpperCase() === 'BAM'
  }
}
