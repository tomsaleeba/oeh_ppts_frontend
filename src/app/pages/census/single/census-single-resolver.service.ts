import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { CensusService, Census } from '../census.service'
import { CommonDataResolver } from '../../common'

@Injectable()
export class CensusDataResolver extends CommonDataResolver<Census> {
  constructor(service: CensusService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
