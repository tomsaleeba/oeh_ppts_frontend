import { NgModule } from '@angular/core'

import { OehComponentsModule } from '../../oehComponents'
import { ThemeModule } from '../../@theme/theme.module'
import { CensusRoutingModule } from './census-routing.module'
import { CensusComponent } from './census.component'
import { CensusAllComponent } from './all/census-all.component'
import { CensusSingleComponent } from './single/census-single.component'
import { CensusService } from './census.service'
import { CensusDataResolver } from './single/census-single-resolver.service'
import { CensusNameListResolver } from './resolvers/census-name-list-resolver.service'

@NgModule({
  imports: [
    ThemeModule,
    CensusRoutingModule,
    OehComponentsModule,
  ],
  declarations: [
    CensusComponent,
    CensusAllComponent,
    CensusSingleComponent,
  ],
  providers: [
    CensusService,
    CensusDataResolver,
    CensusNameListResolver,
  ],
})
export class CensusModule { }
