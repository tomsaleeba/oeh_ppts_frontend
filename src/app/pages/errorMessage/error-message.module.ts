import { NgModule } from '@angular/core'

import { ThemeModule } from '../../@theme/theme.module'
import { ErrorMessageComponent } from './error-message.component'

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    ErrorMessageComponent,
  ],
})
export class ErrorMessageModule { }
