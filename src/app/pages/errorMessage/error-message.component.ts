import { Component } from '@angular/core'
import { Location } from '@angular/common'

@Component({
  templateUrl: './error-message.component.html',
})
export class ErrorMessageComponent {
  constructor(
    private location: Location,
  ) { }

  goBack() {
    this.location.back()
  }
}
