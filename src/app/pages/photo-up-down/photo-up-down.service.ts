import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { CommonService, CommonModel } from '../common/common.service'
import { fullPhotoUpDownSingle } from '../path-fragments'

@Injectable()
export class PhotoUpDownService extends CommonService<PhotoUpDown> {

  constructor(
    http: HttpClient,
  ) {
    super(http, 'photoupdown', fullPhotoUpDownSingle, 'photoud')
  }
}

export class PhotoUpDown extends CommonModel {
  constructor (
    readonly name: string,
    readonly data: string,
    readonly sequence: number,
    readonly transectId: string,
    readonly census: {name: string},
  ) {
    super()
  }
}
