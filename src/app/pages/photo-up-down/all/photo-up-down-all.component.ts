import { Component } from '@angular/core'

import { PhotoUpDownService } from '../photo-up-down.service'

@Component({
  templateUrl: './photo-up-down-all.component.html',
})
export class PhotoUpDownAllComponent {

  constructor(
    public photoUpDownService: PhotoUpDownService,
  ) {}

  settings = [
    {
      field: 'name',
      title: 'Name',
      type: 'id',
    }, {
      field: 'sequence',
      title: 'Sequence',
      type: 'number',
    }, {
      field: 'lat',
      title: 'Latitude',
      type: 'number',
    }, {
      field: 'long',
      title: 'Longitude',
      type: 'number',
    }, {
      field: 'census',
      title: 'Census',
      type: 'link',
    },
  ]
}

