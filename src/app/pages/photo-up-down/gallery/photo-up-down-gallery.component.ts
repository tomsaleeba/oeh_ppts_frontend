import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'

import { Gallery, GalleryItem, ImageItem } from '@ngx-gallery/core'
import { Lightbox } from '@ngx-gallery/lightbox'

import { PhotoUpDownService } from '../photo-up-down.service'
import { CensusService } from '../../census'
import { fullPhotoUpDownSingle, fullCensusSingle, fullCensusAll } from '../../path-fragments'

@Component({
  templateUrl: './photo-up-down-gallery.component.html',
  styles: [`
    .oeh-thumb {
      cursor: pointer;
    }
  `],
})
export class PhotoUpDownGalleryComponent implements OnInit {

  items: GalleryItem[]
  pageSize = 30
  offset = 0
  censusId: string
  censusName: string
  allCensusLink = fullCensusAll
  singleCensusLink = fullCensusSingle
  singleUpDownPhotoLink = fullPhotoUpDownSingle
  isNoPhotos = false
  private thumbnailBaseUrl: string

  constructor(
    public photoUpDownService: PhotoUpDownService,
    public censusService: CensusService,
    public gallery: Gallery,
    public lightbox: Lightbox,
    private route: ActivatedRoute,
  ) {
    gallery.config.thumbHeight = 218
    gallery.config.slidingDirection = 'vertical'
    gallery.config.thumbPosition = 'left'
  }

  ngOnInit () {
    // should be able to forkJoin from rxjs here, but it didn't work
    this.route.data.subscribe(data => {
      this.thumbnailBaseUrl = data.thumbnailBaseUrl
      this.getPhotos().subscribe(photos => {
        this.items = photos.map(item => this.buildImageItem(item))
        this.isNoPhotos = this.items.length === 0
        this.gallery.ref('lightbox').load(this.items)
      })
    })
  }

  onScrollDown () {
    this.offset += this.pageSize
    this.getPhotos().subscribe(data => {
      data.forEach(curr => {
        this.items.push(this.buildImageItem(curr))
      })
    })
  }

  private getPhotos() {
    return new Observable<any>(observer => {
      this.route.paramMap.subscribe(params => {
        this.censusId = params.get('censusId')
        this.censusService.getOne(this.censusId).subscribe(data => {
          this.censusName = data.name
        })
        this.photoUpDownService.getAll({
          limit: this.pageSize,
          skip: this.offset,
          sort: 'sequence ASC',
          census: this.censusId,
        }).subscribe(data => {
          observer.next(data)
        })
      })
    })
  }

  private buildImageItem (item) {
    const objectPathInBucket = item.data
    const fullUrl = `${this.thumbnailBaseUrl}/${objectPathInBucket}`
    const thumbUrl = `${this.thumbnailBaseUrl}/fit-in/218x0/${objectPathInBucket}`
    return new ImageItem({
      src: fullUrl,
      thumb: thumbUrl,
      title: item.name,
      census: item.census.name,
      sequence: item.sequence,
      id: item.id,
    })
  }
}

