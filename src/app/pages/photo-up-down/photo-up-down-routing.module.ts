import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { PhotoUpDownComponent } from './photo-up-down.component'
import { PhotoUpDownAllComponent } from './all/photo-up-down-all.component'
import { PhotoUpDownGalleryComponent } from './gallery/photo-up-down-gallery.component'
import { PhotoUpDownSingleComponent } from './single/photo-up-down-single.component'
import { PhotoUpDownDataResolver } from './single/photo-up-down-single-resolver.service'
import { all, single, gallery, censusIdParam } from '../path-fragments'
import { ThumbnailBaseUrlResolver } from '../common/clientConfig/thumbnail-base-url-resolver.service'

const routes: Routes = [{
  path: '',
  component: PhotoUpDownComponent,
  children: [{
    path: all,
    component: PhotoUpDownAllComponent,
  }, {
    path: `${gallery}/:${censusIdParam}`,
    component: PhotoUpDownGalleryComponent,
    resolve: {
      thumbnailBaseUrl: ThumbnailBaseUrlResolver,
    },
  }, {
    path: single,
    component: PhotoUpDownSingleComponent,
    resolve: {
      photoUpDown: PhotoUpDownDataResolver,
      thumbnailBaseUrl: ThumbnailBaseUrlResolver,
    },
  }],
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhotoUpDownRoutingModule {
}
