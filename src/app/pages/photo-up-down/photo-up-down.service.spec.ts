import { TestBed, inject } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { PhotoUpDownService } from './photo-up-down.service'
import { environment } from '../../../environments/environment'

describe('PhotoUpDownService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PhotoUpDownService],
    })
  })

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify()
  })))

  it('should be created', inject([PhotoUpDownService], (service: PhotoUpDownService) => {
    expect(service).toBeTruthy()
  }))

  it('should return all photoUpDowns', inject([PhotoUpDownService, HttpTestingController],
      (service: PhotoUpDownService, httpMock: HttpTestingController) => {
    service.getAll().subscribe((result) => {
      expect(result.length).toBe(2)
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/photoupdown`)
    expect(req.request.method).toBe('GET')
    req.flush([{id: 1}, {id: 2}])
  }))
})
