import { NgModule } from '@angular/core'

import { GalleryModule } from '@ngx-gallery/core'
import { LightboxModule } from '@ngx-gallery/lightbox'
import { GallerizeModule } from '@ngx-gallery/gallerize'
import { InfiniteScrollModule } from 'ngx-infinite-scroll'

import { OehComponentsModule } from '../../oehComponents'
import { ThemeModule } from '../../@theme/theme.module'
import { PhotoUpDownRoutingModule } from './photo-up-down-routing.module'
import { PhotoUpDownComponent } from './photo-up-down.component'
import { PhotoUpDownAllComponent } from './all/photo-up-down-all.component'
import { PhotoUpDownGalleryComponent } from './gallery/photo-up-down-gallery.component'
import { PhotoUpDownSingleComponent } from './single/photo-up-down-single.component'
import { PhotoUpDownService } from './photo-up-down.service'
import { PhotoUpDownDataResolver } from './single/photo-up-down-single-resolver.service'

@NgModule({
  imports: [
    ThemeModule,
    PhotoUpDownRoutingModule,
    OehComponentsModule,
    InfiniteScrollModule,
    GalleryModule.forRoot(),
    LightboxModule.forRoot(),
    GallerizeModule,
  ],
  declarations: [
    PhotoUpDownComponent,
    PhotoUpDownAllComponent,
    PhotoUpDownGalleryComponent,
    PhotoUpDownSingleComponent,
  ],
  providers: [
    PhotoUpDownService,
    PhotoUpDownDataResolver,
  ],
})
export class PhotoUpDownModule { }
