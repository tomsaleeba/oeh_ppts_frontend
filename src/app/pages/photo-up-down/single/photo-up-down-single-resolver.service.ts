import { Injectable, ErrorHandler } from '@angular/core'
import { Router } from '@angular/router'

import { PhotoUpDownService, PhotoUpDown } from '../photo-up-down.service'
import { CommonDataResolver } from '../../common'

@Injectable()
export class PhotoUpDownDataResolver extends CommonDataResolver<PhotoUpDown> {
  constructor(service: PhotoUpDownService, router: Router, errorHandler: ErrorHandler) {
    super(service, router, errorHandler)
  }
}
