import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'


@Component({
  templateUrl: './photo-up-down-single.component.html',
  styles: [`
    table tbody tr th {
      width: 15em
    }
  `],
})
export class PhotoUpDownSingleComponent implements OnInit {

  photoUpDownData: any
  thumbnailBaseUrl: string

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.photoUpDownData = data.photoUpDown
      this.thumbnailBaseUrl = data.thumbnailBaseUrl
    })
  }

  getPhotoUrlMediumSize () {
    return `${this.thumbnailBaseUrl}/fit-in/900x0/${this.photoUpDownData.data}`
  }

  getPhotoUrlFullSize () {
    return `${this.thumbnailBaseUrl}/${this.photoUpDownData.data}`
  }
}
