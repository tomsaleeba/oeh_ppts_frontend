export const pages = 'pages'
export const dashboard = 'dashboard'
export const notFound = 'not-found'
export const errorMessage = 'error'

export const census = 'census'
export const organisation = 'organisation'
export const photoPanorama = 'photo-panorama'
export const photoUpDown = 'photo-up-down'
export const photopointUser = 'photopoint-users'
export const plot = 'plot'
export const qr = 'qr'
export const studyArea = 'studyarea'

export const censusIdParam = 'censusId'

export const all = 'all'
export const single = 'single'
export const create = 'create'
export const edit = 'edit'
export const bulk = 'bulk'
export const gallery = 'gallery'

export const fullNotFound = `/${pages}/${notFound}`
export const fullErrorMessage = `/${pages}/${errorMessage}`

export const fullCensus = `/${pages}/${census}`
export const fullCensusAll = `/${fullCensus}/${all}`
export const fullCensusSingle = `/${fullCensus}/${single}`
export const fullCensusCreate = `/${fullCensus}/${create}`

export const fullOrganisation = `/${pages}/${organisation}`
export const fullOrganisationAll = `/${fullOrganisation}/${all}`
export const fullOrganisationSingle = `/${fullOrganisation}/${single}`
export const fullOrganisationCreate = `/${fullOrganisation}/${create}`

export const fullPhotoPanorama = `/${pages}/${photoPanorama}`
export const fullPhotoPanoramaAll = `/${fullPhotoPanorama}/${all}`
export const fullPhotoPanoramaSingle = `/${fullPhotoPanorama}/${single}`
export const fullPhotoPanoramaCreate = `/${fullPhotoPanorama}/${create}`
export const fullPhotoPanoramaBulk = `/${fullPhotoPanorama}/${bulk}`

export const fullPhotoUpDown = `/${pages}/${photoUpDown}`
export const fullPhotoUpDownAll = `/${fullPhotoUpDown}/${all}`
export const fullPhotoUpDownGallery = `/${fullPhotoUpDown}/${gallery}`
export const fullPhotoUpDownSingle = `/${fullPhotoUpDown}/${single}`
export const fullPhotoUpDownCreate = `/${fullPhotoUpDown}/${create}`

export const fullPhotopointUser = `/${pages}/${photopointUser}`
export const fullPhotopointUserAll = `/${fullPhotopointUser}/${all}`
export const fullPhotopointUserSingle = `/${fullPhotopointUser}/${single}`
export const fullPhotopointUserCreate = `/${fullPhotopointUser}/${create}`

export const fullPlot = `/${pages}/${plot}`
export const fullPlotAll = `/${fullPlot}/${all}`
export const fullPlotSingle = `/${fullPlot}/${single}`
export const fullPlotCreate = `/${fullPlot}/${create}`

export const fullQr = `/${pages}/${qr}`
export const fullQrAll = `/${fullQr}/${all}`
export const fullQrSingle = `/${fullQr}/${single}`
export const fullQrCreate = `/${fullQr}/${create}`

export const fullStudyArea = `/${pages}/${studyArea}`
export const fullStudyAreaAll = `/${fullStudyArea}/${all}`
export const fullStudyAreaSingle = `/${fullStudyArea}/${single}`
export const fullStudyAreaCreate = `/${fullStudyArea}/${create}`

const vendorPrefix = 'vnd.environment.nsw.gov.au'
export function versionedMime (model, version) {
  return `application/${vendorPrefix}.${model}.${version}+json`
}

export function v1Mime (model) {
  return versionedMime(model, 'v1')
}
