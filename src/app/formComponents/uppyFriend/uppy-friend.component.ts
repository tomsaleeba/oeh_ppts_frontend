import { Component, Input } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core'

@Component({
  selector: 'ngx-uppy-friend',
  templateUrl: './uppy-friend.component.html',
})
export class UppyFriendComponent {

  @Input() fields: FormlyFieldConfig[]
  @Input() model: any
  @Input() form: FormGroup
  @Input() options: FormlyFormOptions

}

