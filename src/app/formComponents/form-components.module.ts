import { NgModule } from '@angular/core'

import { ReactiveFormsModule, FormControl, ValidationErrors } from '@angular/forms'
import { FormlyModule, FormlyFieldConfig } from '@ngx-formly/core'
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap'
import { Observable } from 'rxjs'
import { Data } from '@angular/router'
import { map } from 'rxjs/operators'

import { UppyFriendComponent } from './uppyFriend/uppy-friend.component'
import { OehMultiSelectComponent, idFieldName, labelFieldName } from '../oehComponents'

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormlyBootstrapModule,

    FormlyModule.forRoot({
      types: [
        {
          name: 'oehMultiSelect',
          component: OehMultiSelectComponent,
          wrappers: ['fieldset', 'label'],
        },
      ],
      validators: [
        { name: 'email', validation: emailValidator },
        { name: 'url', validation: urlValidator },
      ],
      validationMessages: [
        { name: 'required', message: 'This field is required' },
        { name: 'maxlength', message: maxlengthValidationMessage },
        { name: 'email', message: 'Email address is not valid' },
        { name: 'url', message: 'Website URL is not valid' },
        { name: 'min', message: minValidationMessage },
        { name: 'max', message: maxValidationMessage },
      ],
    }),
  ],
  exports: [
    ReactiveFormsModule,
    FormlyBootstrapModule,
    FormlyModule,
    UppyFriendComponent,
  ],
  declarations: [
    UppyFriendComponent,
  ],
})
export class FormComponentsModule { }

export function emailValidator(control: FormControl): ValidationErrors {
  // thanks http://emailregex.com/
  // tslint:disable-next-line:max-line-length
  const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const isPassRegex = emailRegex.test(control.value)
  const isValueSupplied = control.value && control.value.length > 0
  return (!isValueSupplied || isPassRegex) ? null : { 'email': true }
}

export function urlValidator(control: FormControl): ValidationErrors {
  // thanks http://urlregex.com/
  // tslint:disable-next-line:max-line-length
  const urlRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/
  const isPassRegex = urlRegex.test(control.value)
  const isValueSupplied = control.value && control.value.length > 0
  return (!isValueSupplied || isPassRegex) ? null : { 'url': true }
}

export function maxlengthValidationMessage(_, field) {
  return `This value should be less than ${field.templateOptions.maxLength} characters`
}

export function minValidationMessage(err, field) {
  return `This value should be more than ${field.templateOptions.min}`
}

export function maxValidationMessage(err, field) {
  return `This value should be less than ${field.templateOptions.max}`
}

export function emailField(key: string, label: string, showValidation: boolean): FormlyFieldConfig {
  return {
    key: key,
    type: 'input',
    validation: {
      show: showValidation,
    },
    templateOptions: {
      type: 'email',
      label: label,
      placeholder: 'Enter email',
      required: false,
      maxLength: 200,
    },
    validators: {
      validation: ['email'],
    },
  }
}

export function urlField(key: string, label: string, showValidation: boolean): FormlyFieldConfig {
  return {
    key: key,
    type: 'input',
    validation: {
      show: showValidation,
    },
    templateOptions: {
      type: 'url',
      label: label,
      placeholder: 'Enter URL',
      required: false,
      maxLength: 500,
    },
    validators: {
      validation: ['url'],
    },
  }
}

export function numberField(key: string, label: string, showValidation: boolean,
    options?: {isRequired?: boolean, min?: number, max?: number}): FormlyFieldConfig {
  options = options || {}
  return {
    key: key,
    type: 'input',
    validation: {
      show: showValidation,
    },
    templateOptions: {
      type: 'number',
      label: label,
      placeholder: 'Enter value',
      required: typeof(options.isRequired) !== 'undefined' ? options.isRequired : false,
      min: typeof(options.min) !== 'undefined' ? options.min : null,
      max: typeof(options.max) !== 'undefined' ? options.max : null,
    },
  }
}

export function plainTextField(key: string, label: string, showValidation: boolean,
    options?: {isRequired?: boolean, maxLength?: number}): FormlyFieldConfig {
  options = options || {}
  return {
    key: key,
    type: 'input',
    validation: {
      show: showValidation,
    },
    templateOptions: {
      label: label,
      placeholder: `Enter ${label.toLowerCase()}`,
      required: typeof(options.isRequired) !== 'undefined' ? options.isRequired : false,
      maxLength: typeof(options.maxLength) !== 'undefined' ? options.maxLength : 500,
    },
  }
}

export function textAreaField(key: string, label: string, showValidation: boolean,
    options?: {maxLength?: number}): FormlyFieldConfig {
  return {
    key: key,
    type: 'textarea',
    validation: {
      show: showValidation,
    },
    templateOptions: {
      label: label,
      rows: 10,
      placeholder: `Description about this ${label.toLowerCase()}`,
      maxLength: typeof(options.maxLength) !== 'undefined' ? options.maxLength : 500,
    },
  }
}
export function selectField(key: string, label: string, optionsObservable: Observable<Data>,
    resolveFieldName: string, options?: {isRequired?: boolean}): FormlyFieldConfig {
  options = options || {}
  return {
    key: key,
    type: 'select',
    templateOptions: {
      label: label,
      options: optionsObservable.pipe(
        map(data => {
          const nullValue = {id: null, name: `(No ${label})`}
          const result = data[resolveFieldName]
          if (!result) {
            console.error(`[select field] no options were supplied to <select> with ` +
              `key='${key}', label='${label}'. Expect explosions to follow...`)
            return result
          }
          result.unshift(nullValue)
          return result
        }),
      ),
      valueProp: 'id',
      labelProp: 'name',
      required: typeof(options.isRequired) !== 'undefined' ? options.isRequired : false,
    },
  }
}

export function multiSelectField(key: string, label: string, optionsObservable: Observable<Data>,
    resolveFieldName: string): FormlyFieldConfig {
  return {
    key: key,
    type: 'oehMultiSelect',
    templateOptions: {
      label: label,
      placeholder: 'Select all that apply, currently nothing selected',
      items: optionsObservable.pipe(
        map(resolveData => {
          const itemData = resolveData[resolveFieldName]
          if (!itemData) {
            throw new Error(`Programmer error: could not find '${resolveFieldName}' in resolve data, we need it!`)
          }
          const result = itemData.reduce((accum, curr) => {
            accum.push({
              [idFieldName]: curr.id,
              [labelFieldName]: curr.name,
            })
            return accum
          }, [])
          return result
        }),
      ),
    },
  }
}
