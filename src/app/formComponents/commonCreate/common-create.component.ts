import { OnInit, ErrorHandler } from '@angular/core'
import { Location } from '@angular/common'
import { FormGroup } from '@angular/forms'
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core'

import { CommonService } from '../../pages/common/common.service'
import { Router, ActivatedRoute } from '@angular/router'

enum states {
  initial,
  loading,
  failed,
  success,
}

export abstract class CommonCreateComponent implements OnInit {

  newId: string
  newRecordLabel: string // something unique to the record to show back to the user
  state = states.initial
  states = states
  isEditMode = false
  isCreateAnother = false
  form = new FormGroup({})
  model: any = {}
  editRecordId: string | number
  options: FormlyFormOptions = {}

  constructor(
    readonly location: Location,
    readonly route: ActivatedRoute,
    readonly router: Router,
    readonly service: CommonService<any>,
    readonly singleRecordRouterLink: string,
    readonly modelNameSingular: string,
    readonly errorHandler: ErrorHandler,
    readonly fields: FormlyFieldConfig[],
  ) { }

  cancel () {
    this.location.back()
  }

  submit(model) {
    this.state = states.loading
    this.newId = null
    let operationObservable
    if (this.isEditMode) {
      operationObservable = this.service.update(this.editRecordId, model)
    } else {
      operationObservable = this.service.create(model)
    }
    operationObservable.subscribe((resp: any) => {
      this.state = states.success
      this.newId = resp.id
      this.newRecordLabel = resp.name
      this.options.resetModel()
      if (!this.isEditMode && this.isCreateAnother) {
        return
      }
      this.router.navigate([this.singleRecordRouterLink], {
        queryParams: {
          id: this.newId,
        },
      }).catch(err => {
        // TODO show error to user
        this.errorHandler.handleError(`Failed while trying to navigate after '${this.getModeLabel()}' to ` +
          `new record with id='${this.newId}', error=${JSON.stringify(err, null, 2)}`)
      })
    }, err => {
      // TODO show error to user
      this.errorHandler.handleError(`Failed while attempting to ${this.getModeLabel()} with ` +
        `data '${JSON.stringify(this.model, null, 2)}', error=${JSON.stringify(err, null, 2)}`)
      this.state = states.failed
    })
  }

  getModeLabel() {
    return this.isEditMode ? 'Edit' : 'Create'
  }

  prepModelForEdit(): void {}

  ngOnInit() {
    // TODO can we get the first field focused?
    this.route.data.subscribe(data => {
      if (!data.existing) {
        return
      }
      this.isEditMode = true
      this.model = data.existing
      this.editRecordId = data.existing.id
      this.stripUneditableFieldsFromModel()
      this.prepModelForEdit()
      this.form.updateValueAndValidity()
    })
  }

  private stripUneditableFieldsFromModel() {
    const editableFields = this.fields.map(e => e.key)
    for (const curr of Object.keys(this.model)) {
      const isEditable = editableFields.indexOf(curr) >= 0
      if (isEditable) {
        continue
      }
      delete this.model[curr]
    }
  }

}

