#!/bin/sh
# generates an environment.ts file from shell environment variables.
# This lets us inject secret-ish (or at least dynamic) values at build time.
set -e
cd `dirname "$0"`

if [ ! -z "$OEH_ENV" ]; then
  echo "[INFO] running with OEH_ENV=$OEH_ENV"
fi

if [ "$OEH_ENV" = "stagingLocal" ]; then
  OEH_IS_PROD=${OEH_IS_PROD:-false}
  OEH_API_URL=${OEH_API_URL:-https://staging.api.oeh.photopoints.environmentalmonitoringgroup.com.au}
  apiKey=${OEH_API_KEY:?}
  OEH_ROLLBAR_ENV=$OEH_ROLLBAR_ENV
  googleMapsApiKey=$OEH_GOOGLE_MAPS_API_KEY
elif [ "$OEH_ENV" = "staging" ]; then
  OEH_IS_PROD=${OEH_IS_PROD:-false}
  OEH_API_URL=${OEH_API_URL:-https://staging.api.oeh.photopoints.environmentalmonitoringgroup.com.au}
  apiKey=${OEH_API_KEY:?}
  OEH_ROLLBAR_ENV=${OEH_ROLLBAR_ENV:-staging}
  googleMapsApiKey=${OEH_GOOGLE_MAPS_API_KEY:?}
elif [ "$OEH_ENV" = "prod" ]; then
  OEH_IS_PROD=${OEH_IS_PROD:-true}
  OEH_API_URL=${OEH_API_URL:-https://api.oeh.photopoints.environmentalmonitoringgroup.com.au}
  apiKey=${OEH_API_KEY:?}
  OEH_ROLLBAR_ENV=${OEH_ROLLBAR_ENV:-production}
  googleMapsApiKey=${OEH_GOOGLE_MAPS_API_KEY:?}
fi

isProd=${OEH_IS_PROD:-false}
apiUrl=${OEH_API_URL:-http://localhost:1337}
rollbarEnv=$OEH_ROLLBAR_ENV
isRollbarEnabled=false
if [ ! -z "$rollbarEnv" ]; then
  isRollbarEnabled=true
fi

if [ "$OEH_ENV" = "test" ]; then
  isProd=false
  apiUrl=http://localhost:1337
  rollbarEnv=
  isRollbarEnabled=false
fi

cat << EOF > environment.ts
// WARNING: this file is generated (by generate-env.sh), do not modify. All changes will be lost!
export const environment = {
  production: $isProd,
  apiUrl: '$apiUrl',
  apiKey: '$apiKey',
  isRollbarEnabled: $isRollbarEnabled,
  rollbarEnv: '$rollbarEnv',
  googleMapsApiKey: '$googleMapsApiKey',
}
EOF
