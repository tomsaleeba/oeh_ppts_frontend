/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}
// it would be nice to move this even earlier in the lifecycle, like on the HTML, but we need the env var
const preheater = new XMLHttpRequest()
preheater.addEventListener('error', function (evt) {
  console.warn('Failed to preheat. Is the target API running a compatible version of code?')
})
preheater.open('GET', `${environment.apiUrl}/preheat`)
preheater.setRequestHeader('x-api-key', environment.apiKey)
preheater.send() // it's async, don't worry

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
