/**
 * BASIC Authentication
 *
 * Simple authentication script intended to be run by Amazon Lambda to
 * provide Basic HTTP Authentication for a static website hosted in an
 * Amazon S3 bucket through Couldfront.
 *
 * https://hackernoon.com/serverless-password-protecting-a-static-website-in-an-aws-s3-bucket-bfaaa01b8666
 * http://kynatro.com/blog/2018/01/03/a-step-by-step-guide-to-creating-a-password-protected-s3-bucket/
 *
 * See .circleci/README.md for notes on how to deploy this.
 */

'use strict'

exports.handler = (event, context, callback) => {

    // Get request and request headers
    const request = event.Records[0].cf.request
    const headers = request.headers

    const users = {
				// TODO add your users here - don't commit sensitive data, edit using the Lambda dashboard to be safe
        // someuser: 'password',
    }
    const isAuthHeader = typeof headers.authorization != 'undefined'
    const isAuthd = isAuthHeader && Object.keys(users).some(currUser => {
        const currPass = users[currUser]
        // Construct the Basic Auth string
        const authString = 'Basic ' + new Buffer(currUser + ':' + currPass).toString('base64')
        const clientValue = headers.authorization[0].value
        return clientValue == authString
    })

    // Require Basic authentication
    if (!isAuthHeader || !isAuthd) {
        const body = 'Unauthorized'
        const response = {
            status: '401',
            statusDescription: 'Unauthorized',
            body: body,
            headers: {
                'www-authenticate': [{key: 'WWW-Authenticate', value:'Basic'}]
            },
        }
        return callback(null, response)
    }

    // Continue request processing if authentication passed
    return callback(null, request)
}
